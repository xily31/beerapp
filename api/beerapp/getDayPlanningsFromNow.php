<?php

require_once("helpers/managerAuth.php");

try {
    //connect as appropriate as above
	$queryString = 'SELECT * FROM `day_planning` WHERE date >= CURRENT_DATE();';
	$statement = $db->prepare($queryString);
	$statement->execute();
	$result = $statement->fetchAll();
	$returnJson = "[";
    foreach($result as $row) {
		$delivRouteUuid = $row["uuid_delivroute"];
		$delivDate = $row["date"];
		$isCanceled = $row["is_canceled"];
		$returnJson .= "{" .
			"\"date\":\"" . strval($delivDate) . "\"," . 
			"\"uuid\":\"" . $delivRouteUuid . "\"," . 
			"\"canceled\":\"" . $isCanceled . "\"},";
    }
	if (count($result) > 0) {
		$returnJson = substr_replace($returnJson, "", -1);
	}
	$returnJson .= "]";
	header('Content-Type: application/json');
	echo $returnJson;
	//die('{"error":"No result."}');
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json');

?>
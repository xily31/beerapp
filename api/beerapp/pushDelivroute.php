<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

if (!isset($_GET["delivrouteJson"])) {
    //die("No delivroute JSON provided.");
}

$delivrouteUuid = null;
$isEdit = false;
if (isset($_GET["delivrouteUuid"])) {
	$delivrouteUuid = $_GET["delivrouteUuid"];
	$isEdit = true;
}

$delivrouteName = "Parcours sans nom";
if (isset($_GET["delivrouteName"])) {
    $delivrouteName = $_GET["delivrouteName"];
}
$delivrouteJson = $_GET["delivrouteJson"];
$uniqueId = ($delivrouteUuid == null ? uniqid("",true) : $delivrouteUuid);
$delivrouteFilename = "deliv_" . $uniqueId . ".json";
$fullDelivPath = "delivroutes/" . $delivrouteFilename;
//header('Content-Type: application/json');

try {
	if ($isEdit) {
		unlink($fullDelivPath);
	}
	$myfile = fopen($fullDelivPath, "w") or die('{"error":"Could not write file ' . $fullDelivPath . '"}');
	$txt = $delivrouteJson;
	fwrite($myfile, $txt);
	fclose($myfile);
	
	if ($isEdit) { // Update existing delivery route
		$statement = $db->prepare(
			'update delivroutes set name = :delivrouteName where uuid = :uuid;'
		);
		$statement->bindParam(":delivrouteName", $delivrouteName);
		$statement->bindParam(":uuid", $uniqueId);
		$statement->execute();
	}
	else { // New delivery route
		$statement = $db->prepare(
			'INSERT INTO delivroutes (json_path, uuid, name) VALUES (:delivrouteFilename, :uuid, :delivrouteName);'
		);
		$statement->bindParam(":delivrouteFilename", $delivrouteFilename);
		$statement->bindParam(":uuid", $uniqueId);
        $statement->bindParam(":delivrouteName", $delivrouteName);
		$statement->execute();
		$delivRouteInsertId = $db->lastInsertId('id');
		
		$statement = $db->prepare(
			'INSERT INTO managers_to_delivroutes (id_manager, id_delivroute) VALUES (:managerId, :delivrouteId);'
		);
		$statement->bindParam(":managerId", $managerId);
		$statement->bindParam(":delivrouteId", $delivRouteInsertId);
		$statement->execute();
	}

	/*var_dump($fullDelivPath);
	var_dump($delivrouteJson);
	var_dump($delivrouteFilename);*/
    //echo $db->lastInsertId('id');
	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    //die ($ex->getMessage());
}

?>
<?php

include_once("helpers/managerAuth.php");

if (!isset($_GET["dpDay"]) || !isset($_GET["dpMonth"]) || !isset($_GET["dpYear"])) {
    die("Not enough data provided");
}

$sqlDate = $_GET["dpYear"] . "-" . $_GET["dpMonth"] . "-" . $_GET["dpDay"];
//header('Content-Type: application/json');

try {
	// Remove entry from delivroutes
    $statement = $db->prepare(
        'DELETE FROM day_planning WHERE date = :sqlDate'
    );
    $statement->bindParam(":sqlDate", $sqlDate);
    $statement->execute();
	
	// TODO : maybe auto-cancel orders that'll be impacted by this deletion?

	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    //die ($ex->getMessage());
}

?>
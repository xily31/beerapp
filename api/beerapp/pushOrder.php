<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'helpers/PHPMailer-master/src/Exception.php';
require 'helpers/PHPMailer-master/src/PHPMailer.php';
require 'helpers/PHPMailer-master/src/SMTP.php';
//include_once("helpers/managerAuth.php");
require_once("helpers/dbConnection.php"); // TODO : check for application token


if (
	!isset($_GET["delivSpotNbr"]) || 
	!isset($_GET["delivrouteUuid"]) || 
	!isset($_GET["orderJson"]) ||
	!isset($_GET["firstName"]) || 
	!isset($_GET["lastName"]) || 
	!isset($_GET["email"]) || 
	!isset($_GET["deliveryDay"])
	) {
    die("No data provided.");
}

$managerId = 1;
$deliverySpotNbr = $_GET["delivSpotNbr"];
$deliveryRouteUuid = $_GET["delivrouteUuid"];
$firstName = $_GET["firstName"];
$lastName = $_GET["lastName"];
$email = $_GET["email"];
$hashPw = "";
$fullDayString = $_GET["deliveryDay"];
$dayArray = explode('-', $fullDayString);
if (count($dayArray) != 3) {
	die(json_encode(array("error", "Wrong day format. YYYY-MM-DD")));
}
$day = $dayArray[2];
$month = $dayArray[1];
$year = $dayArray[0];
$sqlDelivDay = $year . "-" . $month . "-" . $day;

try {
	// Insert new client (or get existing using first & last name and email combination
	$queryString = 'SELECT * FROM `clients` WHERE firstname = :firstName AND lastname = :lastName AND email = :email;';
	$statement = $db->prepare($queryString);
	$statement->bindParam(":firstName", $firstName);
	$statement->bindParam(":lastName", $lastName);
	$statement->bindParam(":email", $email);
	$statement->execute();
	$row = $statement->fetch();
	$clientId = 0;

	if (!$row) {
		$statement = $db->prepare(
			'INSERT INTO clients (firstname, lastname, email, hash_password) 
			VALUES (:firstName, :lastName, :email, :hashPw);'
		);
		$statement->bindParam(":firstName", $firstName);
		$statement->bindParam(":lastName", $lastName);
		$statement->bindParam(":email", $email);
		$statement->bindParam(":hashPw", $hashPw);
		$statement->execute();
		$clientId = $db->lastInsertId('id');
	}
	else {
		$clientId = $row["id"];
	}
	
	if ($clientId == 0) {
		die ("{\"error\":\"" . "Could not get client id" . "\"}");
	}
	
	// Insert into orders
    $statement = $db->prepare(
        'INSERT INTO orders (uuid, id_client, id_manager, delivery_spot_number, order_date, uuid_delivroute, delivery_date) 
		VALUES (uuid(), :clientId, :managerId, :deliverySpotNbr, now(), :delivrouteUuid, :deliveryDay);'
    );
	$statement->bindParam(":clientId", $clientId);
	$statement->bindParam(":managerId", $managerId);
    $statement->bindParam(":deliverySpotNbr", $deliverySpotNbr);
	$statement->bindParam(":delivrouteUuid", $deliveryRouteUuid);
	$statement->bindParam(":deliveryDay", $sqlDelivDay);
    $statement->execute();
    //file_put_contents("delivroutes/" . $delivrouteFilename, $delivrouteJson);
    //echo $db->lastInsertId('id');
	
	// Insert into orders_to_packages
	$orderDbId = $db->lastInsertId('id');
	$jsonString = $_GET["orderJson"];
	$jsonObject = json_decode($jsonString);
	$orders = array();
	foreach ($jsonObject as $myData) {
		$quantity = $myData->quantity;
		$packUuid = $myData->beerPackId;
		$queryString = 'SELECT * FROM `packages` WHERE uuid = :packUuid;';
		//echo "Query is : " . $queryString . "<br/>";
		$statement = $db->prepare($queryString);
		//var_dump($statement);
		$statement->bindParam(":packUuid", $packUuid);
		$statement->execute();
		$row = $statement->fetch();
		$packDbId = $row["id"];
		$orders[$packDbId] = $row;
		$orders[$packDbId]["quantity"] = $quantity;
		//echo "Pack DB id for " . $packUuid . " is " . $packDbId . "<br/>";
		
		$statement = $db->prepare(
			'INSERT INTO orders_to_packages (id_order, id_package, quantity) 
			VALUES (:orderId, :packageId, :quantity);'
		);
		$statement->bindParam(":orderId", $orderDbId);
		$statement->bindParam(":packageId", $packDbId);
		$statement->bindParam(":quantity", $quantity);
		$statement->execute();
	}
	
	// Get delivery postal address
	$queryString = "SELECT json_path FROM `delivroutes` WHERE uuid = :delivUuid;";
	$statement = $db->prepare($queryString);
	$statement->bindParam(":delivUuid", $deliveryRouteUuid);
	$statement->execute();
	$row = $statement->fetch();
	$delivJsonPath = $row["json_path"];
	$delivJson = file_get_contents("delivroutes/" . $delivJsonPath);
	$jsonObject = json_decode($delivJson);
	$delivAddress = NULL;
	$delivTime = NULL;
	foreach ($jsonObject->deliverySpots as $deliverySpotJson) {
		if ($deliverySpotJson->checkpointNumber == $deliverySpotNbr) {
			$delivAddress = $deliverySpotJson->address;
			$delivTime = $deliverySpotJson->utcTime;
			break;
		}
	}
	if ($delivAddress == NULL || $delivTime == NULL) {
		die ("{\"error\":\"" . "Could not find delivery address or time with given deliv info" . "\"}");
	}
	
	// Send order confirmation mail
	$html = "<head><meta http-equiv='Content-Type' content='text/html; charset='UTF-8' /></head>";
	$html .= "<body>";
	$html .= "<h1>Confirmation de commande</h1>";
	
	// 		Delivery
	$html .= "<h2>Livraison</h2>";
	$html .= "<p>Adresse : " . $delivAddress . "</p>";
	$html .= "<p>Heure UTC : " . $delivTime . "</p>";
	
	// 		Order summary
	$html .= "<h2>Produits commandés</h2>";
	$html .= "<ul>";
	$totalPrice = 0;
	foreach ($orders as $orderRow) {
		$packageName = $orderRow["name"];
		$qtyOrdered = $orderRow["quantity"];
		$price = $qtyOrdered * $orderRow["price"];
		$totalPrice += $price;
		$html .= "<li>" . $packageName . " x " . $qtyOrdered . " - " . $price . " €</li>";
	}
	$html .= "</ul>";
	$html .= "<h3>Total TTC : " . $totalPrice . " €</h3>";
	$html .= "</body>";
	//echo $html;
	
	$mail = new PHPMailer;
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.live.com';                       // Specify main and backup server
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'vt-91@hotmail.com';                   // SMTP username
	$mail->Password = 'tastuflette3108';               // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	$mail->Port = 587;                                    //Set the SMTP port number - 587 for authenticated TLS
	$mail->setFrom('vt-91@hotmail.com', 'XiLy Corp');     //Set who the message is to be sent from
	$mail->addReplyTo('vt-91@hotmail.com', 'XiLy Corp');  //Set an alternative reply-to address
	$mail->addAddress($email, $firstName . " " . $lastName);  // Add a recipient
	$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	$mail->isHTML(true);                                  // Set email format to HTML
	$mail->Subject = 'Validation de commande';
	$mail->Body    = $html;
	$mail->AltBody = $html;
	$mail->CharSet = 'UTF-8';
	$mail->msgHTML($html);

	if(!$mail->send()) {
	   die ("{\"error\":\"" . $mail->ErrorInfo . "\"}");
	   exit;
	}
} 
catch(PDOException $ex) {
    die ("{\"error\":\"" . $ex->getMessage() . "\"}");
}

?>
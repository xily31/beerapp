<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

if (!isset($_GET["name"]) || 
   !isset($_GET["typeId"]) ||
   !isset($_GET["description"]) ||
   !isset($_GET["alcohol"]) ||
   !isset($_GET["volume"])) {
	die ("Some data is missing.");
}

$name = $_GET["name"];
$typeId = $_GET["typeId"];
$description = $_GET["description"];
$alcohol = $_GET["alcohol"];
$volume = $_GET["volume"];
$idBrewery = 1;

if (isset($_GET["beerId"])) {
    $id = intval($_GET["beerId"]);
    $queryString = 'UPDATE beers SET name=:name, description=:description, id_beer_type=:id_beer_type, alcohol=:alcohol, id_brewery=:id_brewery, volume=:volume, image_path="" WHERE id=:id;';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":id", $id);
    $statement->bindParam(":name", $name);
    $statement->bindParam(":description", $description);
    $statement->bindParam(":id_beer_type", intval($typeId));
    $statement->bindParam(":alcohol", floatval($alcohol));
    $statement->bindParam(":id_brewery", $idBrewery);
    $statement->bindParam(":volume", intval($volume));
    $statement->execute();
    
    echo '{"result":"ok"}';
    die();
}

try {
    $queryString = 'INSERT INTO beers (name, description, id_beer_type, alcohol, id_brewery, volume, image_path) VALUES (:name, :description, :id_beer_type, :alcohol, :id_brewery, :volume, "");';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":name", $name);
    $statement->bindParam(":description", $description);
    $statement->bindParam(":id_beer_type", intval($typeId));
    $statement->bindParam(":alcohol", floatval($alcohol));
    $statement->bindParam(":id_brewery", $idBrewery);
    $statement->bindParam(":volume", intval($volume));
    $statement->execute();
    //$delivRouteInsertId = $db->lastInsertId('id');
    
	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

?>
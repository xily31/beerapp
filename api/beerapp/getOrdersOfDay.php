<?php

require_once("helpers/managerAuth.php");



if (!isset($_GET["day"])) {
	die(json_encode(array("error", "No day given. YYYY-MM-DD")));
}

$fullDayString = $_GET["day"];
$dayArray = explode('-', $fullDayString);
if (count($dayArray) != 3) {
	die(json_encode(array("error", "Wrong day format. YYYY-MM-DD")));
}
$day = $dayArray[2];
$month = $dayArray[1];
$year = $dayArray[0];
$sqlDay = $year . "-" . $month . "-" . $day;

class PackageOrder { // Is contained among others inside Order
	var $packageName;
	var $packagePrice;
	var $quantity;
	
	function __construct($packageName, $packagePrice, $quantity) {
		$this->packageName = $packageName;
		$this->packagePrice = $packagePrice;
		$this->quantity = $quantity;
	}
	
	function toJson() {
		$jsonArr = array("packageName" => $this->packageName, 
		"packagePrice" => $this->packagePrice,
		"quantity" => $this->quantity);
		return json_encode($jsonArr);
	}
}

class Order { // Single order containing 0-* PackageOrders
	var $id;
	var $uuid;
	var $deliveryDate;
	var $isCanceled;
	var $firstName;
	var $lastName;
	var $email;
	var $packageOrders; // Array of PackageOrder
	var $delivrouteUuid;
	var $isDelivered;
	var $isPrepared;
	
	function __construct($uuid, $id, $deliveryDate, $isCanceled, $firstName, $lastName, $email, $delivrouteUuid, $isDelivered, $isPrepared) {
        $this->id = $id;
        $this->uuid = $uuid;
		$this->deliveryDate = $deliveryDate;
		$this->isCanceled = $isCanceled;
		$this->firstname = $firstName;
		$this->lastName = $lastName;
		$this->email = $email;
		$this->packageOrders = array();
		$this->delivrouteUuid = $delivrouteUuid;
		$this->isDelivered = $isDelivered;
		$this->isPrepared = $isPrepared;
    }
	
	function addPackageOrder($newPackageOrder) {
		array_push($this->packageOrders, $newPackageOrder);
	}
	
	function toJson() {
		$packageOrderJsonArray = array();
		foreach ($this->packageOrders as $po) {
			array_push($packageOrderJsonArray, $po->toJson());
		}
		$packageOrdersJson = json_encode($packageOrderJsonArray, JSON_UNESCAPED_SLASHES);
		$jsonArr = array(
			"uuid" => strval($this->uuid),
			"date" => strval($this->deliveryDate),
			"delivrouteUuid" => $this->delivrouteUuid,
			"firstName" => $this->firstname,
			"lastName" => $this->lastName,
			"email" => $this->email,
			"canceled" => $this->isCanceled, 
			"delivered" => $this->isDelivered, 
			"prepared" => $this->isPrepared, 
			"packageOrders" => $this->packageOrders
		);
		return json_encode($jsonArr, JSON_UNESCAPED_SLASHES);
	}
}

$ordersArray = array();

function findOrderById($arr, $id) {
	foreach ($arr as $orderItem) {
		if ($orderItem->id == $id) {
			return $orderItem;
		}
	}
	return null;
}

try {
    //connect as appropriate as above
	global $ordersArray;
	$queryString = '
	SELECT 
	o.id, o.uuid, o.delivery_date, o.uuid_delivroute, o.is_canceled, o.is_prepared, o.is_delivered, 
	p.name, p.price, 
	c.firstname, c.lastname, c.email,
	dp.uuid_delivroute,
	otp.quantity 
	FROM orders o 
	JOIN orders_to_packages otp ON o.id = otp.id_order 
	JOIN day_planning dp ON o.delivery_date = dp.date 
	JOIN clients c ON o.id_client = c.id 
	JOIN packages p ON otp.id_package = p.id 
	WHERE o.delivery_date = :day
	ORDER BY o.id;
    ';
	$statement = $db->prepare($queryString);
	$statement->bindParam(":day", $fullDayString);
	$statement->execute();
	$result = $statement->fetchAll();
	$returnJson = "[";
    foreach($result as $row) {
		$id = $row["id"];
		$existingOrder = findOrderById($ordersArray, $id);
		
		if (is_null($existingOrder)) {
			// New Orders
			$uuid = $row["uuid"];
			$delivRouteUuid = $row["uuid_delivroute"];
			$delivDate = $row["delivery_date"];
			$isCanceled = $row["is_canceled"];
			$isDelivered = $row["is_delivered"];
			$isPrepared = $row["is_prepared"];
			$packageName = $row["name"];
			$packagePrice = $row["price"];
			$firstName = $row["firstname"];
			$lastName = $row["lastname"];
			$email = $row["email"];
			$quantity = $row["quantity"];
			
			$newOrder = new Order($uuid, $id, $delivDate, $isCanceled, $firstName, $lastName, $email, $delivRouteUuid, $isDelivered, $isPrepared);
			$newPackageOrder = new PackageOrder($packageName, $packagePrice, $quantity);
			
			$newOrder->addPackageOrder($newPackageOrder);
			array_push($ordersArray, $newOrder);
		}
		else {
			// Add OrderPackage to existing Order
			$packageName = $row["name"];
			$packagePrice = $row["price"];
			$quantity = $row["quantity"];
			$newPackageOrder = new PackageOrder($packageName, $packagePrice, $quantity);
			$newOrder->addPackageOrder($newPackageOrder);
		}
    }
	foreach ($ordersArray as $order) {
		$returnJson .= $order->toJson() . ",";
	}
	if (count($result) > 0) {
		$returnJson = substr_replace($returnJson, "", -1);
	}
	$returnJson .= "]";
	echo $returnJson;
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json');

?>
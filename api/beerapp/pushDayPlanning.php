<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

$delivrouteUuid = null;
if (!isset($_GET["delivrouteUuid"]) || 
   !isset($_GET["dpDay"]) ||
   !isset($_GET["dpMonth"]) ||
   !isset($_GET["dpYear"])) {
	die ("Some data is missing.");
}

$delivrouteUuid = $_GET["delivrouteUuid"];
$day = $_GET["dpDay"];
$month = $_GET["dpMonth"];
$year = $_GET["dpYear"];

$sqlDate = $year . "-" . $month . "-" . $day;

try {
    $queryString = 'INSERT INTO day_planning (date, uuid_delivroute) VALUES (:date, :delivrouteUuid);';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":date", $sqlDate);
    $statement->bindParam(":delivrouteUuid", $delivrouteUuid);
    $statement->execute();
    $delivRouteInsertId = $db->lastInsertId('id');
    
	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

?>
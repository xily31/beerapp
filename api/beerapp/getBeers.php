<?php

require_once("helpers/dbConnection.php");

// Get BeerId by GET param
/*if (isset($_GET["brewery_id"])) {
    $beerId = $_GET["brewery_id"];
}
else {
    die('{"error":"Brewery ID was not provided."}');
}*/


try {
    //connect as appropriate as above
	$resultJson = "[";
    foreach($db->query(
        'SELECT b.id, b.name AS beerName, 
        b.alcohol, 
        b.volume, 
        b.description AS beerDescription, 
        bt.name AS beerTypeName
        FROM beers b 
        JOIN beer_types bt ON b.id_beer_type=bt.id '
        //WHERE b.id=1;'
    ) as $row) {
        $resultJson .= "{" .
            '"id":' . $row["id"] . ',' .
            '"name":"' . $row["beerName"] . '",' .
            '"type":"' . $row["beerTypeName"] . '",' . 
            '"description":"' . $row["beerDescription"] . '",' .
            '"volume":"' . $row["volume"] . '",' .
            '"alcohol":"' . $row["alcohol"] . '"' .
            "},";
    }
	$resultJson = substr_replace($resultJson, "", -1);
	$resultJson .= "]";
	if ($resultJson == "]") {
		$resultJson = "[]";
	}
	echo $resultJson;
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

header('Content-Type: application/json; charset=utf-8');

?>
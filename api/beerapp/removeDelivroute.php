<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

if (!isset($_GET["delivrouteUuid"])) {
    die("No delivroute Uuid provided.");
}

$delivrouteUuid = $_GET["delivrouteUuid"];
$delivrouteFilename = "deliv_" . $delivrouteUuid . ".json";
$fullDelivPath = "delivroutes/" . $delivrouteFilename;
//header('Content-Type: application/json');

try {
	// 1. Select db id from 
	$statement = $db->prepare("SELECT id FROM delivroutes WHERE uuid = :delivrouteUuid"); 
	$statement->bindParam(":delivrouteUuid", $delivrouteUuid);
	$statement->execute(); 
	$row = $statement->fetch();
	$delivId = $row["id"];
	
	//echo "id : " . $delivId;
	
	// Remove entry from delivroutes
    $statement = $db->prepare(
        'DELETE FROM delivroutes WHERE id = :delivId'
    );
    $statement->bindParam(":delivId", $delivId);
    $statement->execute();
	
	// Remove delivroute / manager association
    $statement = $db->prepare(
        'DELETE FROM managers_to_delivroutes WHERE id_delivroute = :delivId'
    );
    $statement->bindParam(":delivId", $delivId);
    $statement->execute();
	
	// Remove delivroute / planning association
	$statement = $db->prepare(
        'DELETE FROM day_planning WHERE id_delivroute = :delivId'
    );
    $statement->bindParam(":delivId", $delivId);
    $statement->execute();
	
	unlink($fullDelivPath);

	/*var_dump($fullDelivPath);
	var_dump($delivrouteJson);
	var_dump($delivrouteFilename);*/
    //echo $db->lastInsertId('id');
	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    //die ($ex->getMessage());
}

?>
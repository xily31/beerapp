<?php

require_once("helpers/managerAuth.php");

try {
    //connect as appropriate as above
	$queryString = 'SELECT * FROM `delivroutes` dr
		JOIN managers_to_delivroutes md ON dr.id = md.id_delivroute 
		WHERE md.id_manager = :managerId';
	$statement = $db->prepare($queryString);
	$statement->bindParam(":managerId", $managerId);
	$statement->execute();
	$result = $statement->fetchAll();
	$returnJson = "[";
    foreach($result as $row) {
		$delivPath = "./delivroutes/" . $row["json_path"];
		if (!file_exists($delivPath)) {
			//$returnJson .= '{"error":"Delivroute not found : ' . $delivPath . '"}';
			continue;
		}
		$delivRouteContent = file_get_contents($delivPath);
		$delivRouteUuid = $row["uuid"];
		$delivRouteName = $row["name"];
		$returnJson .= "{" .
			"\"content\":" . $delivRouteContent . "," . 
			"\"name\":\"" . $delivRouteName . "\"," . 
			"\"uuid\":\"" . $delivRouteUuid . "\"},";
    }
	if (count($result) > 0) {
		$returnJson = substr_replace($returnJson, "", -1);
	}
	$returnJson .= "]";
	header('Content-Type: application/json');
	echo $returnJson;
	//die('{"error":"No result."}');
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json; charset=utf-8');

?>
<?php

require_once("helpers/managerAuth.php");



if (!isset($_GET["month"])) {
	die(json_encode(array("error", "No month given.")));
}
if (!isset($_GET["year"])) {
	die(json_encode(array("error", "No year given.")));
}

$month = $_GET["month"];
$year = $_GET["year"];

try {
    //connect as appropriate as above
	global $ordersArray;
	$queryString = 'SELECT DISTINCT delivery_date FROM `orders` WHERE MONTH(delivery_date) = :month AND YEAR(delivery_date) = :year;';
	$statement = $db->prepare($queryString);
	$statement->bindParam(":month", $month);
	$statement->bindParam(":year", $year);
	$statement->execute();
	$result = $statement->fetchAll();
	$returnJson = "[";
    foreach($result as $row) {		
		$dateSqlStr = $row["delivery_date"];
		
		$explodedDate = explode("-", $dateSqlStr);
		if (count($explodedDate) != 3) {
			die("Wrong sql date format returned");
		}
		$day = $explodedDate[2];
		
		$dateObj = '{';
		$dateObj .= '"day":' . strval(intval($day)) . ',';
		$dateObj .= '"month":' . $month . ',';
		$dateObj .= '"year":' . $year;
		$dateObj .= '}';
		$returnJson .= $dateObj . ',';
    }
	if (count($result) > 0) {
		$returnJson = substr_replace($returnJson, "", -1);
	}
	$returnJson .= "]";
	echo $returnJson;
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json');

?>
<?php

require_once("helpers/managerAuth.php");

if (!isset($_GET["uuid"])) {
	die(json_encode(array("error", "Order id not set")));
}
if (!isset($_GET["delivered"]) && !isset($_GET["prepared"])) {
	die(json_encode(array("error", "Status not set")));
}

$orderUuid = $_GET["uuid"];
$isPrepared = $_GET["prepared"];
$isDelivered = $_GET["delivered"];

try {
    //connect as appropriate as above
	global $ordersArray;
	$queryString = '
	UPDATE orders SET is_prepared = :isPrepared, is_delivered = :isDelivered WHERE uuid = :orderUuid;
    ';
	$statement = $db->prepare($queryString);
	$statement->bindParam(":orderUuid", $orderUuid);
	$statement->bindParam(":isPrepared", $isPrepared);
	$statement->bindParam(":isDelivered", $isDelivered);
	
	$statement->execute();
} catch(PDOException $ex) {
	die(json_encode(array("error", "SQL error : " . $ex->getMessage())));
}

header('Content-Type: application/json');

?>
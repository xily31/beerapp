<?php

require_once("helpers/managerAuth.php");

class PackageOrder { // Is contained among others inside Order
	var $packageName;
	var $packagePrice;
	var $quantity;
	
	function __construct($packageName, $packagePrice, $quantity) {
		$this->packageName = $packageName;
		$this->packagePrice = $packagePrice;
		$this->quantity = $quantity;
	}
	
	function toJson() {
		$jsonArr = array("packageName" => $this->packageName, 
		"packagePrice" => $this->packagePrice,
		"quantity" => $this->quantity);
		return json_encode($jsonArr);
	}
}

class Order { // Single order containing 0-* PackageOrders
	var $id;
	var $deliveryDate;
	var $isCanceled;
	var $firstName;
	var $lastName;
	var $email;
	var $packageOrders; // Array of PackageOrder
	var $delivrouteUuid;
	
	function __construct($id, $deliveryDate, $isCanceled, $firstName, $lastName, $email, $delivrouteUuid) {
        $this->id = $id;
		$this->deliveryDate = $deliveryDate;
		$this->isCanceled = $isCanceled;
		$this->firstname = $firstName;
		$this->lastName = $lastName;
		$this->email = $email;
		$this->packageOrders = array();
		$this->delivrouteUuid = $delivrouteUuid;
    }
	
	function addPackageOrder($newPackageOrder) {
		array_push($this->packageOrders, $newPackageOrder);
	}
	
	function toJson() {
		$packageOrderJsonArray = array();
		foreach ($this->packageOrders as $po) {
			array_push($packageOrderJsonArray, $po->toJson());
		}
		$packageOrdersJson = json_encode($packageOrderJsonArray, JSON_UNESCAPED_SLASHES);
		$jsonArr = array(
			"date" => strval($this->deliveryDate),
			"delivrouteUuid" => $this->delivrouteUuid,
			"firstName" => $this->firstname,
			"lastName" => $this->lastName,
			"email" => $this->email,
			"packageOrders" => $this->packageOrders
		);
		return json_encode($jsonArr, JSON_UNESCAPED_SLASHES);
	}
}

$ordersArray = array();

function findOrderById($arr, $id) {
	foreach ($arr as $orderItem) {
		if ($orderItem->id == $id) {
			return $orderItem;
		}
	}
	return null;
}

try {
    //connect as appropriate as above
	global $ordersArray;
	$queryString = '
	SELECT 
	o.id, o.delivery_date, o.uuid_delivroute, o.is_canceled,  
	p.name, p.price, 
	c.firstname, c.lastname, c.email,
	dp.uuid_delivroute,
	otp.quantity 
	FROM orders o 
	JOIN orders_to_packages otp ON o.id = otp.id_order 
	JOIN day_planning dp ON o.delivery_date = dp.date 
	JOIN clients c ON o.id_client = c.id 
	JOIN packages p ON otp.id_package = p.id 
	WHERE o.delivery_date >= CURRENT_DATE();
    ';
	$statement = $db->prepare($queryString);
	$statement->execute();
	$result = $statement->fetchAll();
	$returnJson = "[";
    foreach($result as $row) {
		$id = $row["id"];
		$existingOrder = findOrderById($ordersArray, $id);
		
		if (is_null($existingOrder)) {
			// New Orders
			$delivRouteUuid = $row["uuid_delivroute"];
			$delivDate = $row["delivery_date"];
			$isCanceled = $row["is_canceled"];
			$packageName = $row["name"];
			$packagePrice = $row["price"];
			$firstName = $row["firstname"];
			$lastName = $row["lastname"];
			$email = $row["email"];
			$quantity = $row["quantity"];
			
			$newOrder = new Order($id, $delivDate, $isCanceled, $firstName, $lastName, $email, $delivRouteUuid);
			$newPackageOrder = new PackageOrder($packageName, $packagePrice, $quantity);
			
			$newOrder->addPackageOrder($newPackageOrder);
			array_push($ordersArray, $newOrder);
		}
		else {
			// Add OrderPackage to existing Order
			$packageName = $row["name"];
			$packagePrice = $row["price"];
			$quantity = $row["quantity"];
			$newPackageOrder = new PackageOrder($packageName, $packagePrice, $quantity);
			$newOrder->addPackageOrder($newPackageOrder);
		}
    }
	foreach ($ordersArray as $order) {
		$returnJson .= $order->toJson() . ",";
	}
	if (count($result) > 0) {
		$returnJson = substr_replace($returnJson, "", -1);
	}
	$returnJson .= "]";
	echo $returnJson;
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json');

?>
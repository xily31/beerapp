<?php

require_once("helpers/dbConnection.php");

// Get BeerId by GET param
if (isset($_GET["id"])) {
    $beerId = $_GET["id"];
}
else {
    die('{"error":"Beer ID was not provided."}');
}


try {
    //connect as appropriate as above
    foreach($db->query(
        'SELECT b.name AS beerName, 
        b.alcohol, 
        b.volume, 
        b.description AS beerDescription, 
        bt.name AS beerTypeName
        FROM beers b 
        JOIN beer_types bt ON b.id_beer_type=bt.id 
        WHERE b.id=1;'
    ) as $row) {
        echo "{" .
            '"name":"' . $row["beerName"] . '",' .
            '"type":"' . $row["beerTypeName"] . '",' . 
            '"description":"' . $row["beerDescription"] . '"' .
            "}";
    }
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

header('Content-Type: application/json; charset=utf-8');

?>
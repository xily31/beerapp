<?php

require_once("helpers/dbConnection.php");


try {
    //connect as appropriate as above
	$pkgId = "";
	$resultJson = "[";
    foreach($db->query(
        'SELECT 
		p.id AS pkgId, ptb.id_beer, ptb.id_package, b.id, p.price, 
		p.name AS pkgName, p.uuid, p.description AS pkgDescr, ptb.quantity, b.name AS beerName
		FROM `packages` p
		JOIN packages_to_beers ptb ON p.id = ptb.id_package 
		JOIN beers b ON b.id = ptb.id_beer'
    ) as $row) {
		if ($pkgId != $row["pkgId"]) {
			// End previous entry (close content node)
			if ($resultJson != "[") {
				$resultJson = substr_replace($resultJson, "", -1);
				$resultJson .= ']},';
			}
			
			// Create new entry
			$pkgId = $row["pkgId"];
			$resultJson .= "{" .
				'"name":"' . $row["pkgName"] . '",' .
				'"uuid":"' . $row["uuid"] . '",' .
				'"description":"' . $row["pkgDescr"] . '",' .
                '"price":"' . $row["price"] . '",' .
				'"content":[' . '{"beerName":"' . $row["beerName"] . '","quantity":' . $row["quantity"] . '},';
		}
		else { // Continue building package entry (content node)
			$resultJson .= '{"beerName":"' . $row["beerName"] . '","quantity":' . $row["quantity"] . '},';
		}
    }
	$resultJson = substr_replace($resultJson, "", -1);
	$resultJson .= ']}]';
	echo $resultJson;
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

header('Content-Type: application/json; charset=utf-8');

?>
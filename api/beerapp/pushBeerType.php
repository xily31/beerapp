<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

if (!isset($_GET["name"]) || 
   !isset($_GET["description"])) {
	die ("Some data is missing.");
}
$name = $_GET["name"];
$description = $_GET["description"];
    
if (isset($_GET["id"])) {
    // TODO : implement beer type edition
    //die ("Beer type edition is not implemented yet.");
    $beerId = $_GET["id"];
    
    $queryString = 'UPDATE beer_types SET name=:name, description=:description WHERE id = :id;';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":id", $beerId);
    $statement->bindParam(":name", $name);
    $statement->bindParam(":description", $description);
    $statement->execute();
    
    echo '{"result":"ok"}';
}
else {
    try {
        $queryString = 'INSERT INTO beer_types (name, description) VALUES (:name, :description);';
        $statement = $db->prepare(
            $queryString
        );
        $statement->bindParam(":name", $name);
        $statement->bindParam(":description", $description);
        $statement->execute();

        echo '{"result":"ok"}';
    } catch(PDOException $ex) {
        die ($ex->getMessage());
    }
}

?>
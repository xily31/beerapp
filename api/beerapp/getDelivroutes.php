<?php

require_once("helpers/dbConnection.php");

// Get date by GET param
if (isset($_GET["date"])) {
    $date = $_GET["date"];
}
else {
    die('{"error":"Date was not provided."}');
}

try {
    //connect as appropriate as above
	$query = 'SELECT * FROM `day_planning` dp
		JOIN delivroutes dr ON dr.uuid = dp.uuid_delivroute 
		WHERE date = \'' . $date . '\'';
    foreach($db->query(
        $query
    ) as $row) {
		$delivPath = "./delivroutes/" . $row["json_path"];
		$delivRouteContent = file_get_contents($delivPath);
		$delivRouteUuid = $row["uuid"];
		$delivRouteDate = $row["date"];
		$delivRouteName = $row["name"];
		echo "{" .
			"\"content\":" . $delivRouteContent . "," . 
			"\"uuid\":\"" . $delivRouteUuid . "\"," . 
			"\"name\":\"" . $delivRouteDate . "\"," . 
			"\"date\":\"" . $date . "\"}";
		header('Content-Type: application/json; charset=utf-8');
		return;
    }
	die('{"error":"No result for date ' . $date . ' - ' . $query . '"}');
} catch(PDOException $ex) {
    die ($ex->getMessage() . '\n' . $query);
}

header('Content-Type: application/json; charset=utf-8');

?>
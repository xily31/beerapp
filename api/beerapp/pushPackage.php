<?php

/*error_reporting(-1);
ini_set('display_errors', true);*/

include_once("helpers/managerAuth.php");

if (!isset($_GET["name"]) || 
    !isset($_GET["price"]) || 
    !isset($_GET["description"])) {
	die ("Some data is missing.");
}

$name = $_GET["name"];
$description = $_GET["description"];
$price = $_GET["price"];
$content = array();

if (isset($_GET["contentJson"])) { //[{"id":1,"quantity":3},{...}]
    $contentJson = $_GET["contentJson"];
    $content = json_decode($contentJson);

}

function insertBeerIntoPackage($beerIdToQuantity, $packageId) {
    global $db;
    /*var_dump($beerIdToQuantity);
    var_dump($packageId);*/
    foreach ($beerIdToQuantity as $beerQtyPair) {
        try {
            $beerId = $beerQtyPair->id;
            $beerQty = $beerQtyPair->quantity;
            $queryString = "INSERT INTO packages_to_beers (id_package, id_beer, quantity) VALUES (:id_package, :id_beer, :quantity);";
            $statement = $db->prepare($queryString);
            $statement->bindParam(":id_package", intval($packageId));
            $statement->bindParam(":id_beer", intval($beerId));
            $statement->bindParam(":quantity", intval($beerQty));
            $statement->execute();
        }
        catch (PDOException $ex) {
            die ($ex->getMessage());
        }
    }
}

if (isset($_GET["id"])) {
    $id = intval($_GET["id"]);
    $queryString = 'UPDATE packages SET name=:name, description=:description, price=:price WHERE id=:id;';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":id", $id);
    $statement->bindParam(":name", $name);
    $statement->bindParam(":description", $description);
    $statement->bindParam(":price", floatval($price));
    $statement->execute();
    
    if (count($content) > 0) {
        $queryString = 'DELETE FROM packages_to_beers WHERE id_package=:id;';
        $statement = $db->prepare($queryString);
        $statement->bindParam(":id", $id);
        $statement->execute();
        insertBeerIntoPackage($content, $id);
    }
    
    echo '{"result":"ok"}';
    die();
}

try {
    $queryString = 'INSERT INTO packages (uuid, name, description, price) VALUES (UUID(), :name, :description, :price);';
    $statement = $db->prepare(
        $queryString
    );
    $statement->bindParam(":name", $name);
    $statement->bindParam(":description", $description);
    $statement->bindParam(":price", floatval($price));
    $statement->execute();
    
    $newPackageId = $db->lastInsertId('id');
    insertBeerIntoPackage($content, $newPackageId);
    
	echo '{"result":"ok"}';
} catch(PDOException $ex) {
    die ($ex->getMessage());
}

?>
package com.example.polestar.mapamapamap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.datatype.Duration;

import static com.example.polestar.mapamapamap.DelivRoutesManager.rootUrlStaging;

class DayPlanning {
    public DayPlanning(String year, String month, String day, String delivrouteUuid, Boolean isCanceled) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.delivrouteUuid = delivrouteUuid;
        this.isCanceled = isCanceled;
    }

    public String year;
    public String month;
    public String day;
    public String delivrouteUuid;
    public Boolean isCanceled;
}

public class PlanningActivity extends AppCompatActivity
implements DelivRoutesManager.DelivRoutesDownloadListener,
        DelivRoutesCustomList.DelivRouteChangesListener {

    private FloatingActionButton addFab = null;
    private FloatingActionButton rmFab = null;
    int selectedDay = -1;
    int selectedMonth = -1;
    int selectedYear = -1;
    private DelivRoutesManager delivMgr = null;
    public DelivRoute selectedDelivroute = null;
    final static String pushDayPlanningUrl = "api/beerapp/pushDayPlanning.php";
    final static String removeDayPlanningUrl = "api/beerapp/removeDayPlanning.php";
    final static String getDayPlanningsFromNowUrl = "api/beerapp/getDayPlanningsFromNow.php";
    public static String getMonthPlanningPreviewUrl = "api/beerapp/getMonthPlanningPreview.php";
    public List<DayPlanning> dayPlannings = new ArrayList<>();
    List<Pair<Integer, Integer>> yearsToMonthsPreviewed = new ArrayList<>();

    @Override
    public void onDelivRouteDeleteClicked(DelivRoute dr) {}

    @Override
    public void onDelivRouteEditClicked(DelivRoute dr) {}

    @Override
    public void onDelivRouteSelected(DelivRoute dr) {
        selectedDelivroute = dr;
    }

    @Override public void onDelivRoutesDownloaded() {
        /*addFab.setEnabled(true);
        rmFab.setEnabled(true);*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delivMgr = DelivRoutesManager.getInstance(this, this);
        setContentView(R.layout.activity_planning);
        addFab = findViewById(R.id.planningAct_addEdit);
        rmFab = findViewById(R.id.planningAct_remove);
        addFab.setEnabled(false);
        rmFab.setEnabled(false);
        setupCalendarView();
    }

    private void setupCalendarView() {
        Date currentTime = Calendar.getInstance().getTime();
        updateSelectedDayFromDate(currentTime);
        CompactCalendarView cv = findViewById(R.id.planningAct_calendar);
        final CompactCalendarView finalCv = cv;
        cv.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = finalCv.getEvents(dateClicked);
                Log.d("VTBD", "Day was clicked: " + dateClicked + " with events " + events);
                updateSelectedDayFromDate(dateClicked);
                if (events.size() > 0) {
                    addFab.setEnabled(false);
                    rmFab.setEnabled(true);
                }
                else {
                    addFab.setEnabled(true);
                    rmFab.setEnabled(false);
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d("VTBD", "Month was scrolled to: " + firstDayOfNewMonth);
                updateSelectedDayFromDate(firstDayOfNewMonth);
                getMonthPlanningPreview();
                addFab.setEnabled(false);
                rmFab.setEnabled(false);
            }
        });

        getDayPlanningsFromNow();
        getMonthPlanningPreview();
    }

    public void getMonthPlanningPreview() {
        if (selectedMonth < 1 || selectedMonth > 12) {
            Log.e("VTBD", "getMonthPlanningPreview cannot request, month is " + selectedMonth);
            return;
        }
        for (Pair<Integer, Integer> yearToMonth : yearsToMonthsPreviewed) {
            if (yearToMonth.first == selectedYear && yearToMonth.second == selectedMonth) {
                Log.d("VTBD", "Month Already updated, bypassing.");
                return;
            }
        }
        String url = rootUrlStaging + getMonthPlanningPreviewUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        url += "&month=" + Integer.toString(selectedMonth);
        url += "&year=" + Integer.toString(selectedYear);
        HttpHelper.getQuery(this, url, new EditDelivRouteActivity.HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                CompactCalendarView cv = findViewById(R.id.planningAct_calendar);
                try {
                    JSONArray rootArr = new JSONArray(response);

                    for (int i=0; i<rootArr.length(); ++i) {
                        JSONObject dateObj = rootArr.getJSONObject(i);
                        Integer day = dateObj.getInt("day");
                        Integer month = dateObj.getInt("month");
                        Integer year = dateObj.getInt("year");
                        Event ev1 = new Event(Color.GREEN, DateHelper.getDate(year, month-1, day).getTime(), "Parcours plannifié.");
                        cv.addEvent(ev1);
                        yearsToMonthsPreviewed.add(new Pair<>(year, month));

                        Date selectDate = DateHelper.getDate(selectedYear, selectedMonth-1, selectedDay);
                        if (cv.getEvents(selectDate).size() > 0) {
                            addFab.setEnabled(false);
                            rmFab.setEnabled(true);
                        }
                        else {
                            addFab.setEnabled(true);
                            rmFab.setEnabled(false);
                        }
                    }
                }
                catch (Exception exc) {
                    Log.e("VTBD", "Exception parsing response : " + exc.getMessage());
                }
                cv.invalidate();
            }
        });
    }

    public void updateSelectedDayFromDate(Date d) {
        String day = (String) DateFormat.format("dd", d);
        String month = (String) DateFormat.format("MM", d);
        String year = (String) DateFormat.format("yyyy", d);
        Log.d("VTBD", "DD/MM/YYYY was updated: " + (day) + "/" + (month) + "/" + (year));
        selectedDay = Integer.parseInt(day);
        selectedMonth = Integer.parseInt(month);
        selectedYear = Integer.parseInt(year);
    }

    private void getDayPlanningsFromNow() {
        String url = rootUrlStaging + getDayPlanningsFromNowUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        getQuery(url, new EditDelivRouteActivity.HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                // Parse response
                try {
                    JSONArray rootArr = new JSONArray(response);
                    for (int i=0; i<rootArr.length(); ++i) {
                        JSONObject dayPlanningJson = rootArr.getJSONObject(i);
                        String date = dayPlanningJson.getString("date");
                        StringTokenizer stk = new StringTokenizer(date, "-");
                        if (stk.countTokens() != 3) {
                            Log.e("VTBD", "Day planning date is not properly formatted : " + date);
                            continue;
                        }
                        String year = stk.nextToken();
                        String month = stk.nextToken();
                        String day = stk.nextToken();
                        String delivrouteUuid = dayPlanningJson.getString("uuid");
                        Boolean isCanceled = !dayPlanningJson.getString("canceled").equalsIgnoreCase("0");
                        DayPlanning dpEntry = new DayPlanning(year, month, day, delivrouteUuid, isCanceled);
                        dayPlannings.add(dpEntry);
                    }
                }
                catch (JSONException exc) {
                    Log.e("VTBD", "Json parse exception for getDayPlannings : " + exc.toString());
                }
            }
        });
    }

    public void onAddEditClicked(View v) {
        // Display delivroute selection
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nom du parcours");

        // Set up the input
        final ListView lv = new ListView(this);
        lv.setAdapter(new DelivRoutesCustomList(this, this, delivMgr.delivRoutes, false));
        lv.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for (int j=0; j<adapterView.getChildCount(); ++j) {
                    adapterView.getChildAt(j).setBackgroundColor(Color.TRANSPARENT);
                }
                selectedDelivroute = (DelivRoute)adapterView.getItemAtPosition(i);
                view.setBackgroundColor(getResources().getColor(R.color.colorSelectedRow));
            }
        });
        builder.setView(lv);

        // Set up the buttons
        builder.setPositiveButton("Sélectionner", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedDelivroute != null) {
                    Log.d("VTBD", "Selected delivroute : " + selectedDelivroute.name);
                    // Push day_planning entry
                    String url = rootUrlStaging + pushDayPlanningUrl;
                    url += "?managerId=" + AuthManager.getInstance().getManagerId();
                    url += "&password=" + AuthManager.getInstance().getManagerPassword();
                    url += "&delivrouteUuid=" + selectedDelivroute.uuid;
                    url += "&dpDay=" + selectedDay;
                    url += "&dpMonth=" + selectedMonth;
                    url += "&dpYear=" + selectedYear;
                    final Date addDate = DateHelper.getDate(selectedYear, selectedMonth-1, selectedDay);
                    getQuery(url, new EditDelivRouteActivity.HttpGetCallback() {
                        @Override
                        public void onSuccess(String response) {
                            updateMonthEvents(addDate);
                        }
                    });
                }
                else {
                    Log.e("VTBD", "Could not select delivroute");
                }
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedDelivroute = null;
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void onRemoveClicked(View v) {
        // TODO : prompt before deleting

        // Request entry deletion
        Log.d("VTBD", "Deleting day planning on " + selectedDay + "/" + selectedMonth);
        String url = rootUrlStaging + removeDayPlanningUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        url += "&dpDay=" + selectedDay;
        url += "&dpMonth=" + selectedMonth;
        url += "&dpYear=" + selectedYear;

        final Date deleteDate = DateHelper.getDate(selectedYear, selectedMonth-1, selectedDay);

        getQuery(url, new EditDelivRouteActivity.HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                updateMonthEvents(deleteDate);
            }
        });
    }

    private void updateMonthEvents(final Date month) {
        // Need to update month once entry has been removed
        // TODO : use parameter date instead of selectedMonth/Year
        List<Pair<Integer, Integer>> found = new ArrayList<>();
        for (Pair<Integer, Integer> yearToMonth : yearsToMonthsPreviewed) {
            if (yearToMonth.first == selectedYear && yearToMonth.second == selectedMonth) {
                found.add(yearToMonth);
            }
        }
        yearsToMonthsPreviewed.removeAll(found);

        CompactCalendarView cv = findViewById(R.id.planningAct_calendar);
        List<Event> events = cv.getEventsForMonth(month);
        for (Event ev : events) {
            cv.removeEvent(ev, false);
        }
        getMonthPlanningPreview();
    }

    public void getQuery(String query, final EditDelivRouteActivity.HttpGetCallback callbackInterface) {
        Log.d("VTBD", "http query : " + query);

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("VTBD", "Response is : " + response);
                        if (callbackInterface != null) {
                            callbackInterface.onSuccess(response);
                        }
                    }
                },     new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VTBD", "Volley error : " + error.getMessage());
                try {
                    if (error.networkResponse!= null) {


                        int statusCode = error.networkResponse.statusCode;
                        if (error.networkResponse.data != null) {

                            String body = new String(error.networkResponse.data, "UTF-8");
                            if (statusCode == 400) {


                                JSONObject obj = new JSONObject(body);
                                String errorMsg = obj.getString("message");

                                // getting error msg message may be different according to your API
                                //Display this error msg to user
                                Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();

                                Log.e("VTBD", "error message" + errorMsg);
                            }
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {

                    e.printStackTrace();
                    Log.e("VTBD", "UNKNOWN ERROR :" + e.getMessage());
                    Log.e("VTBD", "Headers :" + error.networkResponse.headers.toString());
                    try {
                        Log.e("VTBD", "Data bytes :" + new String(error.networkResponse.data));
                    } catch (Exception exc) {
                        Log.e("VTBD", "Cannot show byytes:" + exc.getMessage());
                    }
                    Toast.makeText(getApplicationContext(),"Something went Wrong!",Toast.LENGTH_SHORT).show();
                }
            }
        }
        );
        queue.add(stringRequest);
    }
}

package com.example.polestar.mapamapamap;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class BeersCustomList extends ArrayAdapter<Beer> {

    private final Activity context;
    private final List<Beer> beers;
    public boolean showButtons = true;
    BeersChangesListener listener;

    public interface BeersChangesListener {
        void onBeerDeleteClicked(Beer dr);
        void onBeerEditClicked(Beer dr);
        void onBeerSelected(Beer dr);
    }

    public BeersCustomList(Activity context,
                           BeersChangesListener listener,
                                 List<Beer> beers,
                                 boolean showButtons) {
        super(context, R.layout.template_beer_row, beers);
        this.context = context;
        this.beers = beers;
        this.listener = listener;
        this.showButtons = showButtons;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.template_beer_row, null, true);
        TextView nameTv = (TextView) rowView.findViewById(R.id.templ_beerRow_title);
        nameTv.setText(beers.get(position).name);

        final Beer rowItem = beers.get(position);
        FloatingActionButton editBtn = (FloatingActionButton) rowView.findViewById(R.id.templ_beerRow_editBtn);
        FloatingActionButton removeBtn = (FloatingActionButton) rowView.findViewById(R.id.templ_beerRow_removeBtn);



        if (showButtons) {
            // Buttons
            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("VTBD", "Clicked edit button of delivroute item " + rowItem.name);
                    listener.onBeerEditClicked(rowItem);
                }
            });
            removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("VTBD", "Clicked remove button of delivroute item " + rowItem.name);
                    listener.onBeerDeleteClicked(rowItem);
                }
            });
        }
        else {
            editBtn.setVisibility(View.GONE);
            removeBtn.setVisibility(View.GONE);

            // Enable listview item selection if buttons are disabled
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onDelivRouteSelected(rowItem);
                    view.setSelected(true);
                }
            });*/
        }

        return rowView;
    }
}

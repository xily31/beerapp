package com.example.polestar.mapamapamap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.location.Location;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.example.polestar.mapamapamap.DelivRoutesManager.pushDelivrouteUrl;
import static com.example.polestar.mapamapamap.DelivRoutesManager.rootUrlStaging;

/*
 * Google directions API example, using coordinates and waypoints
 * http://maps.googleapis.com/maps/api/directions/json?origin=51,0&destination=51.5,-0.1&sensor=false
 *
 */

class DelivSpotTag {
    public DelivSpotTag(String utcTime, String address) {
        this.address = address;
        this.utcTime = utcTime;
    }
    public String utcTime;
    public String address;
}

public class EditDelivRouteActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener
{

    public GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private Boolean isInsertingWaypoint = false;
    public Polyline route = null;
    Marker selectedMarker = null;
    public List<Marker> markers = new ArrayList<>();
    public static DelivRoute delivrouteToEdit = null;
    private String delivRouteNameToSave = "";
    private String delivSpotTime = "N/A";



    public interface HttpGetCallback {
        public void onSuccess(String response);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(4.0f);
        mMap.setMaxZoomPreference(18.0f);
        //mMap.getUiSettings().setZoomControlsEnabled(true);
        // Move the camera to Toulouse
        LatLng toulouse = new LatLng(43.6, 1.43);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(toulouse));
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnMarkerClickListener(this);
        route = mMap.addPolyline(new PolylineOptions()
                .clickable(true));

        List<LatLng> tlsBoundaries = new ArrayList<>();
        tlsBoundaries.add(new LatLng(43.646738, 1.433478)); // Toulouse North Boundary
        tlsBoundaries.add(new LatLng(43.573370, 1.452201)); // Toulouse South Boundary
        tlsBoundaries.add(new LatLng(43.586000, 1.405570)); // Toulouse West Boundary
        tlsBoundaries.add(new LatLng(43.593773, 1.492581)); // Toulouse East Boundary

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng point : tlsBoundaries) {
            builder.include(point);
        }
        final LatLngBounds.Builder finalBuilder = builder;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(finalBuilder.build(), 4));
                checkForDelivrouteToEdit();
                mMap.getUiSettings().setMapToolbarEnabled(false);
            }
        });
    }

    private void checkForDelivrouteToEdit() {
        if (delivrouteToEdit != null) {
            delivrouteToEdit.drawDelivRoute(this);
        }
    }

    public void onEditWaypointClicked(View v) {
        // TODO : address edit

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Point de livraison");

        View formView = getLayoutInflater().inflate(R.layout.form_edit_delivspot, null);
        final Spinner spHour = formView.findViewById(R.id.delivspotForm_hour);
        final Spinner spMinute = formView.findViewById(R.id.delivspotForm_minute);
        if (selectedMarker.getTag() instanceof DelivSpotTag) {
            DelivSpotTag dst = (DelivSpotTag)selectedMarker.getTag();
            // TODO : check that its {0-9}(2):{0-9}(2)
            String hourSaved = dst.utcTime.split(":")[0];
            for (int i=0; i<spHour.getCount(); ++i) {
                String str = spHour.getItemAtPosition(i).toString();
                if (hourSaved.equalsIgnoreCase(str)) {
                    spHour.setSelection(i);
                }
            }

            String minuteSaved = dst.utcTime.split(":")[1];
            for (int i=0; i<spMinute.getCount(); ++i) {
                String str = spMinute.getItemAtPosition(i).toString();
                if (minuteSaved.equalsIgnoreCase(str)) {
                    spMinute.setSelection(i);
                }
            }
        }
        builder.setView(formView);

        // Set up the buttons
        builder.setPositiveButton("Appliquer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    String timeString = spHour.getSelectedItem().toString() + ":" + spMinute.getSelectedItem().toString();
                    delivSpotTime = timeString;
                }
                catch (NullPointerException exc) {
                    Log.e("VTBD", "Spinner hour/minute values are null");
                    delivSpotTime = "";
                }
                // TODO : get address from form
                selectedMarker.setTag(new DelivSpotTag(delivSpotTime, selectedMarker.getTitle()));

                Log.d("VTBD", "Saved deliv spot");
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void undoWaypoint(View view) {
        updateInsertWaypointButton(false);
        // TODO: Delete last waypoint
    }

    public void zoomToCurrentLocation(View view) {
        try
        {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                Log.d("STATE", "Received location " + location.toString());
                                // Add a marker in current location and move the camera
                                LatLng curLoc = new LatLng(location.getLatitude(), location.getLongitude());
                                //mMap.addMarker(new MarkerOptions().position(curLoc).title("You are here"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curLoc, 16.0f));
                            }
                        }
                    });
        }
        catch (SecurityException e) {
            Log.e("VTBD", "Could not get user loc : " + e.toString());
        }
    }

    @Override
    public void onMapClick(LatLng point) {
        Log.d("VTBD", "On map click");

        if (isInsertingWaypoint) {
            if (selectedMarker != null) {
                for (int i=0; i<markers.size(); ++i) {
                    Marker m = markers.get(i);
                    if (m.equals(selectedMarker)){
                        addNewWaypoint(point, i+1);
                    }
                }
            }
            updateInsertWaypointButton(false);
        }

        if (selectedMarker != null) {
            selectedMarker = null;
        }
        updateDelivSpotButtons();
    }

    @Override
    public void onMapLongClick(LatLng point) {
        updateInsertWaypointButton(false);
        Log.d("VTBD", "On map long click");
        addNewWaypoint(point, null);
    }

    @Override
    public boolean onMarkerClick(Marker m) {
        updateInsertWaypointButton(false);
        Log.d("VTBD", "On marker click : " + m.toString());
        selectedMarker = m;
        if (selectedMarker.getTag() != null && selectedMarker.getTag() instanceof DelivSpotTag) {
            DelivSpotTag dst = (DelivSpotTag)selectedMarker.getTag();
            Log.d("VTBD", "Selected deliv spot " + dst.address + " - " + dst.utcTime);
        }
        updateDelivSpotButtons();
        return false;
    }

    public void addNewWaypoint(LatLng point, Integer pos) {
        updateInsertWaypointButton(false);
        Log.d("VTBD", "Added waypoint at " + point.toString());
        String pointName = point.toString();
        Marker newMarker = mMap.addMarker((new MarkerOptions()
                .position(point)
                .title(pointName)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow_down))));
        if (pos == null) {
            markers.add(newMarker);
        }
        else {
            markers.add(pos, newMarker);
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        updateTracedRoute();
    }

    public void getPostalAddressForMarker(final Marker m) {
        String latLngString = m.getPosition().latitude + "," + m.getPosition().longitude;
        // TODO : get base url from public static, get API key from strings xml
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLngString + "&key=AIzaSyBvF961bgKhLMMDVQBExLDl6RTcI9CiuEA";
        getQuery(url, new HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject rootObj = new JSONObject(response);
                    JSONArray results = rootObj.getJSONArray("results");
                    if (results.length() >= 1) {
                        JSONObject firstResult = results.getJSONObject(0);
                        String formattedAddr = firstResult.getString("formatted_address");
                        updateMarkerAddress(m, formattedAddr);
                    }
                    else {
                        Log.w("VTBD", "Directions returned no results for " + m.getPosition().toString());
                    }
                }
                catch (Exception e) {
                    Log.e("VTBD", "Error fetching directions api response : " + e.getMessage());
                }
            }
        });
    }

    public void updateMarkerAddress(final Marker m, final String postalAddress) {
        Log.d("VTBD", "Updating address for marker " + m.getTitle() + " to " + postalAddress);
        Marker foundMarker = getMarkerByLatLng(m.getPosition());
        if (foundMarker != null) {
            foundMarker.setTitle(postalAddress);
            selectedMarker.hideInfoWindow();
            selectedMarker.showInfoWindow();
        }
        else {
            Log.e("VTBD", "No marker found for " + m.getTitle() + ", " + m.getPosition());
        }
    }

    public List<LatLng> getWaypointsFromMarkers() {
        List<LatLng> waypoints = new ArrayList<>();
        for (Marker m : markers) {
            waypoints.add(new LatLng(m.getPosition().latitude, m.getPosition().longitude));
        }
        return waypoints;
    }

    private void updateTracedRoute() {
        route.setPoints(getWaypointsFromMarkers());
    }

    private void updateInsertWaypointButton(Boolean isInserting) {
        if (isInserting != null) {
            isInsertingWaypoint = isInserting;
        }
        FloatingActionButton addWaypointBtn = findViewById(R.id.editDelivAct_insertWaypoint);
        if (isInsertingWaypoint) {
            //addWaypointBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            addWaypointBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_green_light)));
        }
        else{
            //addWaypointBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            addWaypointBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_green_dark)));
        }
    }

    private void updateDelivSpotButtons() {
        isInsertingWaypoint = false;
        FloatingActionButton delivSpotBtn = findViewById(R.id.btn_setAsDelivSpot);
        FloatingActionButton rmWaypointBtn = findViewById(R.id.editDelivAct_rmWaypoint);
        FloatingActionButton addWaypointBtn = findViewById(R.id.editDelivAct_insertWaypoint);
        FloatingActionButton editWaypointBtn = findViewById(R.id.editDelivAct_editWaypoint);
        if (selectedMarker != null) {
            delivSpotBtn.setVisibility(View.VISIBLE);
            rmWaypointBtn.setVisibility(View.VISIBLE);
            addWaypointBtn.setVisibility(View.VISIBLE);
            editWaypointBtn.setVisibility(View.VISIBLE);
            // Set as marker button
            if (selectedMarker.getTag() != null && selectedMarker.getTag() instanceof DelivSpotTag) {
                delivSpotBtn.setImageResource(R.drawable.ic_shop_off);
            }
            else {
                // No tag means it's not a delivery spot
                delivSpotBtn.setImageResource(R.drawable.ic_shop_on);
                editWaypointBtn.setVisibility(View.GONE); // We only edit delivery spots
            }
        }
        else {
            delivSpotBtn.setVisibility(View.GONE);
            rmWaypointBtn.setVisibility(View.GONE);
            addWaypointBtn.setVisibility(View.GONE);
            editWaypointBtn.setVisibility(View.GONE);
        }
    }

    public void onInsertWaypointClicked(View v) {
        updateInsertWaypointButton(!isInsertingWaypoint); // Toggle
    }

    private JSONObject buildDelivRouteJson() {
        JSONObject rootJson = new JSONObject();
        JSONArray routeJson = new JSONArray();
        JSONArray delivSpotsJson = new JSONArray();
        try {
            // Build deliv route JSON
            List<LatLng> waypoints = getWaypointsFromMarkers();
            for (int i = 0; i < waypoints.size(); ++i) {
                LatLng wp = waypoints.get(i);
                Marker m = getMarkerByLatLng(wp);
                JSONObject waypointJson = new JSONObject();
                Integer cpNumber = i + 1;
                waypointJson.put("number", cpNumber);
                waypointJson.put("latitude", wp.latitude);
                waypointJson.put("longitude", wp.longitude);
                routeJson.put(waypointJson);
                if (m != null) {
                    if (m.getTag() != null && m.getTag() instanceof DelivSpotTag) {
                        DelivSpotTag dst = (DelivSpotTag)m.getTag();
                        Log.d("VTBD", "DelivSpot found : " + m.getTitle());
                        String delivHour = dst.utcTime;
                        // TODO : get address from googlem aps API (actually .getTitle())
                        String address = Double.toString(wp.latitude) + ", " + Double.toString(wp.longitude);
                        JSONObject dsJson = new JSONObject();
                        dsJson.put("checkpointNumber", cpNumber);
                        dsJson.put("utcTime", delivHour);
                        dsJson.put("address", address);
                        delivSpotsJson.put(dsJson);
                    }
                }
            }
            rootJson.put("checkpoints", routeJson);
            rootJson.put("deliverySpots", delivSpotsJson);
            return rootJson;
        }
        catch (JSONException e) {
            Log.e("VTBD", "Cannot build delivroute JSON, exc : " + e.toString());
            return null;
        }
    }

    private void pushDelivRoute(JSONObject delivrouteJson) {

        // Send to server
        String url = rootUrlStaging + pushDelivrouteUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        if (delivRouteNameToSave != null) {
            url += "&delivrouteName=" + Uri.encode(delivRouteNameToSave);
        }
        if (delivrouteToEdit != null) {
            url += "&delivrouteUuid=" + delivrouteToEdit.uuid;
        }
        url += "&delivrouteJson=" + Uri.encode(delivrouteJson.toString());
        getQuery(url, new HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                Intent intent = new Intent(EditDelivRouteActivity.this, DelivRoutesActivity.class);
                intent.putExtra("toast", "Parcours " + new String(delivRouteNameToSave) + "sauvegardé avec succès.");
                startActivity(intent);
            }
        });
    }

    public void saveDelivRoute(View view) {
        updateInsertWaypointButton(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nom du parcours");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        if (delivrouteToEdit != null) {
            input.setText(delivrouteToEdit.name);
        }
        input.requestFocus();
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Sauvegarder", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delivRouteNameToSave = input.getText().toString();
                JSONObject delivJson = buildDelivRouteJson();
                pushDelivRoute(delivJson);
                Log.d("VTBD", "saveDelivRoute done");
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void removeWaypointClicked(View v) {
        updateInsertWaypointButton(false);
        if (selectedMarker == null) {
            return;
        }
        markers.remove(selectedMarker);
        selectedMarker.remove();
        selectedMarker = null;
        updateTracedRoute();
        updateDelivSpotButtons();
    }

    private Marker getMarkerByLatLng(LatLng ll) {
        for (Marker m : markers) {
            if (m.getPosition().longitude == ll.longitude && m.getPosition().latitude == ll.latitude) {
                return m;
            }
        }
        return null;
    }

    /*
    public void traceRoute(View view) {
        Polyline polyline1 = mMap.addPolyline(new PolylineOptions().clickable(true));
        route.setPoints(waypoints);
        String encodedWaypoints = PolyUtil.encode(waypoints);
        Log.d("VTBD", "First waypoint " + waypoints.get(0).toString());
        Log.d("VTBD", "Encoded waypoints : " + encodedWaypoints);
        Log.d("VTBD", "Last waypoint " + waypoints.get(waypoints.size()-1).toString());
        // TODO : enable billing and get "server API key" from google, otherwise this can't be used
        //getDirections();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng point : waypoints) {
            builder.include(point);
        }
        // Use this if we need to limit bounds of map
        //mMap.setLatLngBoundsForCameraTarget(builder.build());
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 10));
    }
    */

    public List<Integer> getLatestDeliveryTime() {
        if (markers.isEmpty()) {
            return null;
        }
        // Get latest deliv time on the delivroute
        Integer latestHour = 0;
        for (Marker m : markers) {
            if (m.getTag() != null && m.getTag() instanceof DelivSpotTag) {
                DelivSpotTag dst = (DelivSpotTag)m.getTag();
                String hourSaved = dst.utcTime.split(":")[0];
                Integer hourInt = Integer.parseInt(hourSaved);
                if (hourInt > latestHour) {
                    latestHour = hourInt;
                }
            }
        }
        Integer latestMinute = 0;
        for (Marker m : markers) {
            if (m.getTag() != null && m.getTag() instanceof DelivSpotTag) {
                DelivSpotTag dst = (DelivSpotTag)m.getTag();
                String hourSaved = dst.utcTime.split(":")[0];
                if (Integer.parseInt(hourSaved) == latestHour) {
                    String minuteSaved = dst.utcTime.split(":")[1];
                    Integer minuteInt = Integer.parseInt(minuteSaved);
                    if (minuteInt > latestHour) {
                        latestMinute = minuteInt;
                    }
                }
            }
        }
        List<Integer> ret = new ArrayList<>();
        ret.add(latestHour);
        ret.add(latestMinute);
        return ret;
    }

    public void onSetDelivSpotClicked(View v) {
        updateInsertWaypointButton(false);
        if (selectedMarker != null) {
            if (selectedMarker.getTag() != null) { // If is deliv spot, set it as normal waypoint
                selectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow_down));
                selectedMarker.setTag(null); // Remove is deliv spot tag
                selectedMarker.setTitle(selectedMarker.getPosition().toString());
                selectedMarker.hideInfoWindow();
                selectedMarker.showInfoWindow();
            }
            else { // Otherwise, set it as deliv spot
                selectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_waypoint));
                selectedMarker.setTitle("PDL : " + selectedMarker.getPosition());

                // New delivery spot hour = last deliv spot hour + 30m
                List<Integer> latestDelivTime = getLatestDeliveryTime();
                String utcTime = "08:00";
                if (latestDelivTime != null && latestDelivTime.size() == 2) {
                    Integer delivSpotMinute = latestDelivTime.get(1);
                    delivSpotMinute += 30;
                    Integer delivSpotHour = latestDelivTime.get(0);
                    Log.d("VTBD", "Minutes after addition : "  + delivSpotMinute.toString());
                    Log.d("VTBD", "Hours after addition before % : "  + delivSpotHour.toString());
                    if (delivSpotMinute >= 60) {
                        delivSpotHour += 1;
                        delivSpotMinute = delivSpotMinute % 60;
                    }
                    Log.d("VTBD", "Latest hour : " +
                            delivSpotHour.toString() + ":" + delivSpotMinute.toString());
                    utcTime = delivSpotHour.toString() + ":" + delivSpotMinute.toString();
                    if (delivSpotMinute == 0) {
                        utcTime += "0";
                    }
                }

                selectedMarker.setTag(new DelivSpotTag(utcTime, selectedMarker.getTitle())); // Add is deliv spot tag
                getPostalAddressForMarker(selectedMarker);
            }
        }
        else {
            Log.e("VTBD", "onSetDelivSpotClicked error : selectedMarker is null");
        }
        updateDelivSpotButtons();
    }

    public void getQuery(String query, final HttpGetCallback callbackInterface) {
        Log.d("VTBD", "http query : " + query);

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("VTBD", "Response is : " + response);
                        if (callbackInterface != null) {
                            callbackInterface.onSuccess(response);
                        }
                    }
                },     new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VTBD", "Volley error : " + error.getMessage());
                try {
                    if (error.networkResponse!= null) {


                        int statusCode = error.networkResponse.statusCode;
                        if (error.networkResponse.data != null) {

                            String body = new String(error.networkResponse.data, "UTF-8");
                            if (statusCode == 400) {


                                JSONObject obj = new JSONObject(body);
                                String errorMsg = obj.getString("message");

                                // getting error msg message may be different according to your API
                                //Display this error msg to user
                                Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();

                                Log.e("VTBD", "error message" + errorMsg);
                            }
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {

                    e.printStackTrace();
                    Log.e("VTBD", "UNKNOWN ERROR :" + e.getMessage());
                    Log.e("VTBD", "Headers :" + error.networkResponse.headers.toString());
                    try {
                        Log.e("VTBD", "Data bytes :" + new String(error.networkResponse.data));
                    } catch (Exception exc) {
                        Log.e("VTBD", "Cannot show byytes:" + exc.getMessage());
                    }
                    Toast.makeText(getApplicationContext(),"Something went Wrong!",Toast.LENGTH_SHORT).show();
                }
            }
        }
        );
        queue.add(stringRequest);
    }
}

/* TEST API
First waypoint lat/lng: (43.56397112696249,1.387261301279068)
Encoded waypoints : yq{hGk}mGhuEsmBm|DeeGs}E}n@
Last waypoint lat/lng: (43.595611225554435,1.4545834064483645)

https://maps.googleapis.com/maps/api/directions/json?origin=43.56397112696249,1.387261301279068&destination=43.595611225554435,1.4545834064483645&sensor=false&waypoints=via:enc:yq{hGk}mGhuEsmBm|DeeGs}E}n@:&key=AIzaSyDadMAOxUm7KE1QEae7DDBWRptFwvTt5t4
 */


    /*
    public void getDirections() {
        LatLng firstPoint = waypoints.get(0);
        LatLng lastPoint = waypoints.get(waypoints.size()-1);
        String query = "https://maps.googleapis.com/maps/api/directions/json" +
                "?origin=" + firstPoint.latitude + "," + firstPoint.longitude +
                "&destination=" + lastPoint.latitude + "," + lastPoint.longitude +
                "&waypoints=via:enc:" + PolyUtil.encode(waypoints) + ":" +
                "&sensor=false" +
                "&key=AIzaSyDadMAOxUm7KE1QEae7DDBWRptFwvTt5t4";
        Log.d("VTBD", "Query directions : " + query);
        getQuery(query);
    }
    */

    /*
    public void postQuery(String url) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            String URL = url;
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("Title", "Android Volley Demo");
            jsonBody.put("Author", "BNK");
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    */
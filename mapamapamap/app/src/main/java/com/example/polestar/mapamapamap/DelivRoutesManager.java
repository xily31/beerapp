package com.example.polestar.mapamapamap;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DelivRoutesManager {
    private static DelivRoutesManager singleInstance = null;
    public List<DelivRoute> delivRoutes = new ArrayList<>();
    private Context context;
    public List<DelivRoutesDownloadListener> listeners = new ArrayList<>();
    final static String rootUrlStaging = "http://vps577075.ovh.net:8899/";
    final static String rootUrlLocal = "http://192.168.1.59/";
    final static String pushDelivrouteUrl = "api/beerapp/pushDelivroute.php";
    private Boolean isDownloadInProgress = false;

    public interface DelivRoutesDownloadListener {
        public void onDelivRoutesDownloaded();
    }

    private void notifyDownloadListeners() {
        for (DelivRoutesDownloadListener l : listeners) {
            l.onDelivRoutesDownloaded();
        }
    }

    private DelivRoutesManager(Context c) {
        this.context = c;
        downloadDelivRoutes();
    }

    public static DelivRoutesManager getInstance(Context c, DelivRoutesDownloadListener listener) {
        if (singleInstance == null) {
            singleInstance = new DelivRoutesManager(c);
        }
        singleInstance.listeners.add(listener);
        return singleInstance;
    }

    public void downloadDelivRoutes() {
        if (isDownloadInProgress) {
            Log.d("VTBD", "downloadDelivRoutes already in progress");
            return;
        }
        isDownloadInProgress = true;
        delivRoutes.clear();
        AuthManager am = AuthManager.getInstance();
        RequestQueue queue = Volley.newRequestQueue(this.context);
        String url = rootUrlStaging + "api/beerapp/getManagerDelivroutes.php";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("managerId", am.getManagerId());
        builder.appendQueryParameter("password", am.getManagerPassword());
        /*url += "?managerId=" + am.getManagerId();
        url += "&password=" + am.getManagerPassword();*/
        Log.d("VTBD", "Requesting url : " + builder.toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, builder.toString(),
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // Display the first 500 characters of the response string.
                    Log.d("VTBD", "Response is : " + response);
                    try {
                        JSONArray delivRoutesArray = new JSONArray(response);
                        for (int i = 0; i<delivRoutesArray.length(); ++i) {
                            DelivRoute newDr = new DelivRoute(delivRoutesArray.getJSONObject(i));
                            delivRoutes.add(newDr);
                        }
                        isDownloadInProgress = false;
                        notifyDownloadListeners();
                    }
                    catch (Exception e) {
                        Log.e("VTBD","Error receiving delivroutes data : " + e.toString());
                        e.printStackTrace();
                        isDownloadInProgress = false;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    isDownloadInProgress = false;
                    Log.e("VTBD", "ERROR downloading delivroutes is : " + error.toString());
                    error.printStackTrace();
                    CharSequence text = "Erreur de connexion.";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
            );
        queue.add(stringRequest);
    }

    public void removeDelivroute(String uuid) {
        delivRoutes.clear();
        AuthManager am = AuthManager.getInstance();
        RequestQueue queue = Volley.newRequestQueue(this.context);
        String url = rootUrlStaging + "api/beerapp/removeDelivroute.php";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("managerId", am.getManagerId());
        builder.appendQueryParameter("password", am.getManagerPassword());
        builder.appendQueryParameter("delivrouteUuid", uuid);
        Log.d("VTBD", "Requesting url : " + builder.toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, builder.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Log.d("VTBD", "Response is : " + response);
                        Log.d("VTBD", "Download delivroutes success");
                        downloadDelivRoutes(); // Re download delivroutes and notify listeners
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isDownloadInProgress = false;
                Log.e("VTBD", "ERROR downloading delivroutes is : " + error.toString());
                error.printStackTrace();
            }
        }
        );
        queue.add(stringRequest);
    }
}

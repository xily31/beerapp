package com.example.polestar.mapamapamap;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class BeersManager {
    private static BeersManager singleInstance = null;
    public List<Beer> beers = new ArrayList<>();
    private Context context;
    public List<BeersDownloadListener> listeners = new ArrayList<>();
    final static String rootUrlStaging = "http://vps577075.ovh.net:8899/";
    final static String rootUrlLocal = "http://192.168.1.59/";
    final static String pushBeerUrl = "api/beerapp/pushBeer.php";
    private Boolean isDownloadInProgress = false;

    public interface BeersDownloadListener {
        void onBeersDownloaded();
    }

    private void notifyDownloadListeners() {
        for (BeersDownloadListener l : listeners) {
            l.onBeersDownloaded();
        }
    }

    private BeersManager(Context c) {
        this.context = c;
        downloadBeers();
    }

    public static BeersManager getInstance(Context c, BeersDownloadListener listener) {
        if (singleInstance == null) {
            singleInstance = new BeersManager(c);
        }
        singleInstance.listeners.add(listener);
        return singleInstance;
    }

    public void downloadBeers() {
        if (isDownloadInProgress) {
            Log.d("VTBD", "downloadBeers already in progress");
            return;
        }
        isDownloadInProgress = true;
        beers.clear();
        AuthManager am = AuthManager.getInstance();
        RequestQueue queue = Volley.newRequestQueue(this.context);
        String url = rootUrlStaging + "api/beerapp/getBeers.php";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("managerId", am.getManagerId());
        builder.appendQueryParameter("password", am.getManagerPassword());
        /*url += "?managerId=" + am.getManagerId();
        url += "&password=" + am.getManagerPassword();*/
        Log.d("VTBD", "Requesting url : " + builder.toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, builder.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("VTBD", "Response is : " + response);
                        try {
                            JSONArray beersArray = new JSONArray(response);
                            for (int i = 0; i<beersArray.length(); ++i) {
                                Beer newBeer = new Beer(beersArray.getJSONObject(i));
                                beers.add(newBeer);
                            }
                            isDownloadInProgress = false;
                            notifyDownloadListeners();
                        }
                        catch (Exception e) {
                            Log.e("VTBD","Error receiving beers data : " + e.toString());
                            e.printStackTrace();
                            isDownloadInProgress = false;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isDownloadInProgress = false;
                Log.e("VTBD", "ERROR downloading beers is : " + error.toString());
                error.printStackTrace();
                CharSequence text = "Erreur de connexion.";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        );
        queue.add(stringRequest);
    }

    public void removeBeer(Integer beerId) {
        beers.clear();
        AuthManager am = AuthManager.getInstance();
        RequestQueue queue = Volley.newRequestQueue(this.context);
        String url = rootUrlStaging + "api/beerapp/removeBeer.php";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("managerId", am.getManagerId());
        builder.appendQueryParameter("password", am.getManagerPassword());
        builder.appendQueryParameter("beerId", beerId.toString());
        Log.d("VTBD", "Requesting url : " + builder.toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, builder.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Log.d("VTBD", "Response is : " + response);
                        Log.d("VTBD", "Download beers success");
                        downloadBeers(); // Re download delivroutes and notify listeners
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isDownloadInProgress = false;
                Log.e("VTBD", "ERROR downloading beers is : " + error.toString());
                error.printStackTrace();
            }
        }
        );
        queue.add(stringRequest);
    }
}

package com.example.polestar.mapamapamap;

import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DelivRoute {
    public String contentJson;
    public String uuid;
    public String name;
    public List<Checkpoint> checkpoints = new ArrayList<>();

    public class Checkpoint {
        public Double latitude;
        public Double longitude;
        public Integer number;
        public DeliverySpot deliverySpot = null;

        public Checkpoint(Integer number, Double latitude, Double longitude) {
            this.number = number;
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public class DeliverySpot {
        public Integer number;
        public String time;
        public String address;

        public DeliverySpot(Integer number, String time, String address) {
            this.number = number;
            this.time = time;
            this.address = address;
        }
    }

    public DelivRoute(JSONObject json) {
        this.contentJson = json.toString();
        try {
            JSONObject rootObj = json;
            this.uuid = rootObj.getString("uuid");
            this.name = rootObj.getString("name");
            JSONObject contentObj = rootObj.getJSONObject("content");
            JSONArray checkpointsArr = contentObj.getJSONArray("checkpoints");
            JSONArray delivSpotsArr = contentObj.getJSONArray("deliverySpots");
            for (int j=0; j<checkpointsArr.length(); ++j) {
                JSONObject checkpointObj = checkpointsArr.getJSONObject(j);
                Integer cpNbr = checkpointObj.getInt("number");
                Double cpLat = checkpointObj.getDouble("latitude");
                Double cpLon = checkpointObj.getDouble("longitude");
                Checkpoint newCp = new Checkpoint(cpNbr, cpLat, cpLon);
                this.checkpoints.add(newCp);
            }
            for (int j=0; j<delivSpotsArr.length(); ++j) {
                JSONObject delivSpotObj = delivSpotsArr.getJSONObject(j);
                Integer dsNbr = delivSpotObj.getInt("checkpointNumber");
                String dsTime = delivSpotObj.getString("utcTime");
                String dsAddr = delivSpotObj.getString("address");
                DeliverySpot newDs = new DeliverySpot(dsNbr, dsTime, dsAddr);
                Checkpoint cp = getCheckpointByNumber(dsNbr);
                if (cp != null) {
                    cp.deliverySpot = newDs;
                }
            }
        }
        catch (Exception e) {
            Log.e("VTBD", "Route JSON parsing error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Checkpoint getCheckpointByNumber(Integer nbr) {
        for (Checkpoint cp : checkpoints) {
            if (cp.number.equals(nbr)) return cp;
        }
        return null;
    }

    public void drawDelivRoute(EditDelivRouteActivity editActivity) {
        List<LatLng> waypoints = editActivity.getWaypointsFromMarkers();
        GoogleMap map = editActivity.mMap;
        Polyline route = editActivity.route;
        for (Checkpoint cp : checkpoints) {
            LatLng point = new LatLng(cp.latitude, cp.longitude);
            String pointName = Double.toString(cp.latitude) + ", " + Double.toString(cp.latitude);
            Marker newMarker = map.addMarker((new MarkerOptions()
                    .position(point)
                    .title(pointName)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_arrow_down))
            ));
            if (cp.deliverySpot != null) {
                DelivSpotTag dst = new DelivSpotTag(cp.deliverySpot.time, cp.deliverySpot.address);
                newMarker.setTag(dst);
                newMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_waypoint));
                newMarker.setTitle("Livré à " + cp.deliverySpot.time + " : " + newMarker.getTitle());
            }
            editActivity.markers.add(newMarker);
            waypoints.add(point);
        }
        route.setPoints(waypoints);
    }
}
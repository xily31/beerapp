package com.example.polestar.mapamapamap;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DayOrdersCustomList  extends ArrayAdapter<DayOrder> {

    interface DayOrdersChangesListener {
        void onOrderPrepareButtonClicked(final DayOrder dayOrder);
        void onOrderDeliverButtonClicked(final DayOrder dayOrder);
    }

    private final Activity context;
    private final List<DayOrder> dayOrders;
    DayOrdersChangesListener listener;

    public DayOrdersCustomList(Activity context,
                            DayOrdersChangesListener listener,
                            List<DayOrder> dayOrders) {
        super(context, R.layout.template_order_row, dayOrders);
        this.context = context;
        this.dayOrders = dayOrders;
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.template_order_row, null, true);
        DayOrder dayOrder = dayOrders.get(position);
        TextView nameTv = (TextView) rowView.findViewById(R.id.orderRow_title);
        String title = dayOrder.firstName + " " + dayOrder.lastName;
        nameTv.setText(title);
        TextView descrTv = (TextView) rowView.findViewById(R.id.orderRow_descr);
        TextView ttcTv = (TextView) rowView.findViewById(R.id.orderRow_ttc);
        Double ttc = 0.;
        String descr = "";
        for (int i=0; i<dayOrder.packageOrders.size(); ++i) {
            PackageOrder po = dayOrder.packageOrders.get(i);
            descr += po.quantity.toString() + " " + po.packageName + "\n";
            ttc += po.packagePrice * po.quantity;
        }
        descrTv.setText(descr);
        ttcTv.setText(ttc.toString() + "€");

        final DayOrder rowItem = dayOrders.get(position);
        final DayOrder.DayOrderStatusListener dayOrderListener = (DayOrder.DayOrderStatusListener)this;
        FloatingActionButton prepareBtn = rowView.findViewById(R.id.orderRow_validate);
        FloatingActionButton deliverBtn = rowView.findViewById(R.id.orderRow_deliver);

        // Buttons
        prepareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked check button of order item " + rowItem.firstName);
                listener.onOrderPrepareButtonClicked(rowItem);
            }
        });
        deliverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked check button of order item " + rowItem.firstName);
                listener.onOrderDeliverButtonClicked(rowItem);
            }
        });

        if (dayOrder.isPrepared) {
            prepareBtn.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(android.R.color.holo_green_light)));
        }
        if (dayOrder.isDelivered) {
            deliverBtn.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(android.R.color.holo_green_light)));
        }

        return rowView;
    }
}

package com.example.polestar.mapamapamap;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class BeersActivity
        extends AppCompatActivity
        implements BeersManager.BeersDownloadListener, BeersCustomList.BeersChangesListener {

    BeersManager beersManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beers);
        beersManager = BeersManager.getInstance(this, this);
        beersManager.downloadBeers();
    }

    @Override
    public void onBeersDownloaded() {
        ListView lv = (ListView) findViewById(R.id.beersAct_list);
        lv.setAdapter(new BeersCustomList(
                BeersActivity.this,
                this,
                beersManager.beers,
                true)
        );
    }

    @Override
    public void onBeerDeleteClicked(Beer dr) {

    }

    @Override
    public void onBeerEditClicked(Beer dr) {

    }

    @Override
    public void onBeerSelected(Beer dr) {

    }
}

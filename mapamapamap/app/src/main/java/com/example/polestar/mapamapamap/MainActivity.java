package com.example.polestar.mapamapamap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user taps Display Map button */
    public void switchToMap(View view) {
        Intent intent = new Intent(MainActivity.this, DelivRoutesActivity.class);
        startActivity(intent);
    }

    public void onPlanningClicked(View v) {
        Intent intent = new Intent(MainActivity.this, PlanningActivity.class);
        startActivity(intent);
    }

    public void onOrdersClicked(View v) {
        Intent intent = new Intent(MainActivity.this, OrdersActivity.class);
        startActivity(intent);
    }

    public void onInventoryClicked(View v) {
        Intent intent = new Intent(MainActivity.this, InventoryActivity.class);
        startActivity(intent);
    }
}

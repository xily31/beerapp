package com.example.polestar.mapamapamap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class InventoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
    }

    public void onBeersClicked(View v) {
        Intent intent = new Intent(InventoryActivity.this, BeersActivity.class);
        startActivity(intent);
    }

    public void onPackagesClicked(View v) {

    }
}

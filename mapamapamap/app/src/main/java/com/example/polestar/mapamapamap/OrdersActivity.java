package com.example.polestar.mapamapamap;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.CalendarView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.polestar.mapamapamap.DelivRoutesManager.rootUrlStaging;

public class OrdersActivity extends AppCompatActivity
implements EditDelivRouteActivity.HttpGetCallback {

    public static String getMonthOrdersPreviewUrl = "api/beerapp/getMonthOrdersPreview.php";

    int selectedDay = -1;
    int selectedMonth = -1;
    int selectedYear = -1;

    List<Pair<Integer, Integer>> yearsToMonthsPreviewed = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        Date currentTime = Calendar.getInstance().getTime();
        updateSelectedDayFromDate(currentTime);

        // Calendar setup
        CompactCalendarView cv = findViewById(R.id.calendarView);
        // define a listener to receive callbacks when certain events happen.
        final CompactCalendarView finalCv = cv;
        cv.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = finalCv.getEvents(dateClicked);
                Log.d("VTBD", "Day was clicked: " + dateClicked + " with events " + events);
                updateSelectedDayFromDate(dateClicked);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d("VTBD", "Month was scrolled to: " + firstDayOfNewMonth);
                updateSelectedDayFromDate(firstDayOfNewMonth);
                getMonthOrdersPreview();
            }
        });

        // Highlight days with orders
        getMonthOrdersPreview();
    }

    public void updateSelectedDayFromDate(Date d) {
        String day = (String) DateFormat.format("dd", d);
        String month = (String) DateFormat.format("MM", d);
        String year = (String) DateFormat.format("yyyy", d);
        Log.d("VTBD", "DD/MM/YYYY was updated: " + (day) + "/" + (month) + "/" + (year));
        selectedDay = Integer.parseInt(day);
        selectedMonth = Integer.parseInt(month);
        selectedYear = Integer.parseInt(year);
    }

    public void onDayOrdersClicked(View v) {
        Intent intent = new Intent(OrdersActivity.this, DayOrdersActivity.class);
        intent.putExtra("d", selectedDay);
        intent.putExtra("m", selectedMonth);
        intent.putExtra("y", selectedYear);
        startActivity(intent);
    }

    private void getMonthOrdersPreview() {
        if (selectedMonth < 1 || selectedMonth > 12) {
            Log.e("VTBD", "getMonthOrdersPreview cannot request, month is " + selectedMonth);
            return;
        }
        for (Pair<Integer, Integer> yearToMonth : yearsToMonthsPreviewed) {
            if (yearToMonth.first == selectedYear && yearToMonth.second == selectedMonth) {
                Log.d("VTBD", "Month Already updated, bypassing.");
                return;
            }
        }
        String url = rootUrlStaging + getMonthOrdersPreviewUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        url += "&month=" + Integer.toString(selectedMonth);
        url += "&year=" + Integer.toString(selectedYear);
        HttpHelper.getQuery(this, url, this);
    }

    @Override
    public void onSuccess(String response) { // Response should be a json array of dates {"day":int, "month":int, "year":int} that have 1 or more orders
        CompactCalendarView cv = findViewById(R.id.calendarView);
        try {
            JSONArray rootArr = new JSONArray(response);

            for (int i=0; i<rootArr.length(); ++i) {
                JSONObject dateObj = rootArr.getJSONObject(i);
                Integer day = dateObj.getInt("day");
                Integer month = dateObj.getInt("month");
                Integer year = dateObj.getInt("year");
                Event ev1 = new Event(Color.GREEN, DateHelper.getDate(year, month-1, day).getTime(), "Commandes à ce jour.");
                cv.addEvent(ev1);
                yearsToMonthsPreviewed.add(new Pair<>(year, month));
            }
        }
        catch (Exception exc) {
            Log.e("VTBD", "Exception parsing response : " + exc.getMessage());
        }
        cv.invalidate();
    }
}

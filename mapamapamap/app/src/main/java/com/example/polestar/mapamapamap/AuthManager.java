package com.example.polestar.mapamapamap;

public class AuthManager {
    private static AuthManager singleInstance = null;
    private String managerName = "alessandro"; // TODO : read from prefs
    private String managerId = "1";
    private String managerPassword = "managerPassword"; // TODO : encore, use hash

    private AuthManager() {

    }

    public static AuthManager getInstance() {
        if (singleInstance == null) {
            singleInstance = new AuthManager();
        }
        return singleInstance;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public String getManagerId() {
        return managerId;
    }
}

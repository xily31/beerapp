package com.example.polestar.mapamapamap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class DelivRoutesActivity extends AppCompatActivity implements DelivRoutesCustomList.DelivRouteChangesListener, DelivRoutesManager.DelivRoutesDownloadListener {
    DelivRoutesManager delivRoutesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliv_routes);
        Context cont = getApplicationContext();
        delivRoutesManager = DelivRoutesManager.getInstance(cont, this);
        updateDelivRoutesView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getExtras() != null && intent.getExtras().containsKey("toast")) {
            Toast.makeText(getApplicationContext(), intent.getStringExtra("toast"), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        delivRoutesManager.downloadDelivRoutes();
        updateDelivRoutesView();
        EditDelivRouteActivity.delivrouteToEdit = null;
    }

    @Override
    public void onDelivRoutesDownloaded() {
        updateDelivRoutesView();
    }

    @Override
    public void onDelivRouteDeleteClicked(DelivRoute dr) {
        final DelivRoute delivRouteCopy = dr;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        // Delete delivroute from server and redownload delivroutes
                        Log.d("VTBD", "onDelivRouteDeleteClicked for delivroute " + delivRouteCopy.uuid);
                        delivRoutesManager.removeDelivroute(delivRouteCopy.uuid);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Êtes vous sûr de vouloir supprimer le parcours ?").setPositiveButton("Oui", dialogClickListener)
                .setNegativeButton("Non", dialogClickListener).show();
    }

    @Override
    public void onDelivRouteEditClicked(DelivRoute dr) {
        EditDelivRouteActivity.delivrouteToEdit = dr;
        Intent intent = new Intent(DelivRoutesActivity.this, EditDelivRouteActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDelivRouteSelected(DelivRoute dr) {}

    private void updateDelivRoutesView() {
        ListView lv = (ListView) findViewById(R.id.delivRoutesAct_listview);
        lv.setAdapter(new DelivRoutesCustomList(
                DelivRoutesActivity.this,
                this,
                delivRoutesManager.delivRoutes,
                true)
            );
    }

    public void onCreateDelivRouteClicked(View v) {
        Intent intent = new Intent(DelivRoutesActivity.this, EditDelivRouteActivity.class);
        startActivity(intent);
    }
}

package com.example.polestar.mapamapamap;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class HttpHelper {

    public static void getQuery(Activity act, String query, final EditDelivRouteActivity.HttpGetCallback callbackInterface) {
        Log.d("VTBD", "http query : " + query);

        RequestQueue queue = Volley.newRequestQueue(act.getBaseContext());
        final Activity finalAct = act;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("VTBD", "Response is : " + response);
                        if (callbackInterface != null) {
                            callbackInterface.onSuccess(response);
                        }
                    }
                },     new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VTBD", "Volley error : " + error.getMessage());
                try {
                    if (error.networkResponse!= null) {


                        int statusCode = error.networkResponse.statusCode;
                        if (error.networkResponse.data != null) {

                            String body = new String(error.networkResponse.data, "UTF-8");
                            if (statusCode == 400) {


                                JSONObject obj = new JSONObject(body);
                                String errorMsg = obj.getString("message");

                                // getting error msg message may be different according to your API
                                //Display this error msg to user
                                Toast.makeText(finalAct.getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();

                                Log.e("VTBD", "error message" + errorMsg);
                            }
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {

                    e.printStackTrace();
                    Log.e("VTBD", "UNKNOWN ERROR :" + e.getMessage());
                    Log.e("VTBD", "Headers :" + error.networkResponse.headers.toString());
                    try {
                        Log.e("VTBD", "Data bytes :" + new String(error.networkResponse.data));
                    } catch (Exception exc) {
                        Log.e("VTBD", "Cannot show byytes:" + exc.getMessage());
                    }
                    Toast.makeText(finalAct.getApplicationContext(),"Something went Wrong!",Toast.LENGTH_SHORT).show();
                }
            }
        }
        );
        queue.add(stringRequest);
    }
}

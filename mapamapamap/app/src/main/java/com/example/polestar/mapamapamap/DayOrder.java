package com.example.polestar.mapamapamap;

import android.app.Activity;

import java.util.List;

import static com.example.polestar.mapamapamap.DelivRoutesManager.rootUrlStaging;

public class DayOrder implements EditDelivRouteActivity.HttpGetCallback {

    public interface DayOrderStatusListener {
        public void onOrderStatusUpdated(DayOrder d);
    }

    public static String updateDayOrderUrl = "api/beerapp/updateOrderStatus.php";
    private DayOrderStatusListener listener = null;

    public DayOrder(String uuid, String delivDate, String delivRouteUuid, String firstName,
                    String lastName, String email, List<PackageOrder> packageOrders, Boolean isCanceled,
                    Boolean isDelivered, Boolean isPrepared) {
        this.uuid = uuid;
        this.delivDate = delivDate;
        this.delivRouteUuid = delivRouteUuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.packageOrders = packageOrders;
        this.isCanceled = isCanceled;
        this.isPrepared = isPrepared;
        this.isDelivered = isDelivered;
    }

    public String uuid;
    public String delivDate;
    public String delivRouteUuid;
    public String firstName;
    public String lastName;
    public String email;
    public List<PackageOrder> packageOrders;
    public Boolean isCanceled;
    public Boolean isDelivered;
    public Boolean isPrepared;

    @Override
    public void onSuccess(String response) {
        if (listener != null) {
            listener.onOrderStatusUpdated(this);
        }
    }

    public void updateStatus(Activity act, DayOrderStatusListener listener) {
        this.listener = listener;
        String query = rootUrlStaging + updateDayOrderUrl;
        query += "?managerId=" + AuthManager.getInstance().getManagerId();
        query += "&password=" + AuthManager.getInstance().getManagerPassword();
        query += "&uuid=" + this.uuid;
        query += "&prepared=" + (this.isPrepared ? "1" : "0");
        query += "&delivered=" + (this.isDelivered ? "1" : "0");
        HttpHelper.getQuery(act,query, this);
    }
}
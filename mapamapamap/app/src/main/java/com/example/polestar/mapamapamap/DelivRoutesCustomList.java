package com.example.polestar.mapamapamap;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


public class DelivRoutesCustomList extends ArrayAdapter<DelivRoute> {

    private final Activity context;
    private final List<DelivRoute> delivRoutes;
    public boolean showButtons = true;
    DelivRouteChangesListener listener;

    public interface DelivRouteChangesListener {
        void onDelivRouteDeleteClicked(DelivRoute dr);
        void onDelivRouteEditClicked(DelivRoute dr);
        void onDelivRouteSelected(DelivRoute dr);
    }

    public DelivRoutesCustomList(Activity context,
                            DelivRouteChangesListener listener,
                            List<DelivRoute> delivRoutes,
                            boolean showButtons) {
        super(context, R.layout.template_delivroute_row, delivRoutes);
        this.context = context;
        this.delivRoutes = delivRoutes;
        this.listener = listener;
        this.showButtons = showButtons;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.template_delivroute_row, null, true);
        TextView nameTv = (TextView) rowView.findViewById(R.id.templ_delivRouteRow_name);
        nameTv.setText(delivRoutes.get(position).name);

        final DelivRoute rowItem = delivRoutes.get(position);
        Button editBtn = (Button) rowView.findViewById(R.id.templ_delivRouteRow_editBtn);
        Button removeBtn = (Button) rowView.findViewById(R.id.templ_delivRouteRow_removeBtn);



        if (showButtons) {
            // Buttons
            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("VTBD", "Clicked edit button of delivroute item " + rowItem.name);
                    listener.onDelivRouteEditClicked(rowItem);
                }
            });
            removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("VTBD", "Clicked remove button of delivroute item " + rowItem.name);
                    listener.onDelivRouteDeleteClicked(rowItem);
                }
            });
        }
        else {
            editBtn.setVisibility(View.GONE);
            removeBtn.setVisibility(View.GONE);

            // Enable listview item selection if buttons are disabled
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onDelivRouteSelected(rowItem);
                    view.setSelected(true);
                }
            });*/
        }

        return rowView;
    }
}

package com.example.polestar.mapamapamap;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Beer {

    class BeerType {
        public String name;
        public String description;

        public BeerType(String name, String description) {
            this.name = name;
            this.description = description;
        }
    }

    public String name;
    public Double alcohol;
    public BeerType type;
    public Integer volume;
    public Integer id;
    public String description;

    public Beer(JSONObject obj) {
        try {
            this.id = obj.getInt("id");
            this.name = obj.getString("name");
            this.description = obj.getString("description");
            this.type = new BeerType(obj.getString("type"), "");
            this.alcohol = Double.parseDouble(obj.getString("alcohol"));
            this.volume = Integer.parseInt(obj.getString("volume"));
        }
        catch (JSONException e) {
            Log.e("VTBD", e.getMessage());
        }
    }

    public Beer(String name, Double alcohol, BeerType type, Integer volume, Integer id, String description) {
        this.name = name;
        this.alcohol = alcohol;
        this.type = type;
        this.volume = volume;
        this.id = id;
        this.description = description;
    }
}

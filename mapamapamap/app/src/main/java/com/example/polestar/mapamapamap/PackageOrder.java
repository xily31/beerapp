package com.example.polestar.mapamapamap;

public class PackageOrder {

    public PackageOrder(String packageName, Double packagePrice, Integer quantity) {
        this.packageName = packageName;
        this.packagePrice = packagePrice;
        this.quantity = quantity;
    }

    public String packageName;
    public Double packagePrice;
    public Integer quantity;
}
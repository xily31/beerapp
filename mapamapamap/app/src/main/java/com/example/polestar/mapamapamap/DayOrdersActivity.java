package com.example.polestar.mapamapamap;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static com.example.polestar.mapamapamap.DelivRoutesManager.rootUrlStaging;

public class DayOrdersActivity extends AppCompatActivity
        implements DayOrdersCustomList.DayOrdersChangesListener, DayOrder.DayOrderStatusListener {
    List<DayOrder> dayOrders = new ArrayList<>();
    final static String getDayOrdersUrl = "api/beerapp/getOrdersOfDay.php";
    int day = -1;
    int month = -1;
    int year = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_orders);
        Intent intent = getIntent();
        day = intent.getIntExtra("d", -1);
        month = intent.getIntExtra("m", -1);
        year = intent.getIntExtra("y", -1);
        if (day == -1 || month == -1 || year == -1) {
            Log.e("VTBD", "Day order date is wrong! d, m, y : " +
                    Integer.toString(day) + ", " +
                    Integer.toString(month) + ", " +
                    Integer.toString(year));
        }
        else {
            Log.d("VTBD", "Order date is " + Integer.toString(day) + "/" +
                Integer.toString(month) + "/" + Integer.toString(year));
        }

        // TODO : get orders and display them in listview with adapter class
        getDayOrders();
    }

    @Override
    public void onOrderPrepareButtonClicked(final DayOrder fd) {
        Log.d("VTBD", "onOrderPrepareButtonClicked in activity, order : " + fd.uuid);
        DayOrder d = getDayOrderByUuid(fd.uuid);
        if (d != null) {
            d.isPrepared = !d.isPrepared;
            d.updateStatus(this, this);
        }
    }

    @Override
    public void onOrderDeliverButtonClicked(final DayOrder fd) {
        Log.d("VTBD", "onOrderDeliverButtonClicked in activity, order : " + fd.uuid);
        DayOrder d = getDayOrderByUuid(fd.uuid);
        if (d != null) {
            d.isDelivered = !d.isDelivered;
            d.updateStatus(this, this);
        }
    }

    @Override
    public void onOrderStatusUpdated(DayOrder d) {
        ListView lv = findViewById(R.id.dayOrdersAct_ordersList);
        lv.setAdapter(new DayOrdersCustomList(this, this, this.dayOrders));
    }

    private DayOrder getDayOrderByUuid(String uuid) {
        for (DayOrder d : dayOrders) {
            if (d.uuid == uuid) {
                return d;
            }
        }
        return null;
    }

    private void getDayOrders() {
        String url = rootUrlStaging + getDayOrdersUrl;
        url += "?managerId=" + AuthManager.getInstance().getManagerId();
        url += "&password=" + AuthManager.getInstance().getManagerPassword();
        url += "&day=" + Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day);
        final Activity thisAct = (Activity)this;
        final DayOrdersCustomList.DayOrdersChangesListener thisListener = (DayOrdersCustomList.DayOrdersChangesListener)this;
        getQuery(url, new EditDelivRouteActivity.HttpGetCallback() {
            @Override
            public void onSuccess(String response) {
                // Parse response
                try {
                    JSONArray rootArr = new JSONArray(response);
                    for (int i=0; i<rootArr.length(); ++i) {
                        JSONObject dayOrderJson = rootArr.getJSONObject(i);
                        String uuid = dayOrderJson.getString("uuid");
                        Boolean isCanceled = !dayOrderJson.getString("canceled").equalsIgnoreCase("0");
                        Boolean isDelivered = !dayOrderJson.getString("delivered").equalsIgnoreCase("0");
                        Boolean isPrepared = !dayOrderJson.getString("prepared").equalsIgnoreCase("0");
                        String delivRouteUuid = dayOrderJson.getString("delivrouteUuid");
                        String firstName = dayOrderJson.getString("firstName");
                        String lastName = dayOrderJson.getString("lastName");
                        String email = dayOrderJson.getString("email");
                        JSONArray packageOrdersJson = dayOrderJson.getJSONArray("packageOrders");
                        List<PackageOrder> packageOrders = new ArrayList<>();
                        for (int j=0; j<packageOrdersJson.length(); ++j) {
                            JSONObject packageOrderJson = packageOrdersJson.getJSONObject(j);
                            String packageName = packageOrderJson.getString("packageName");
                            Double packagePrice = packageOrderJson.getDouble("packagePrice");
                            Integer quantity = packageOrderJson.getInt("quantity");
                            packageOrders.add(new PackageOrder(packageName, packagePrice, quantity));
                        }
                        String delivDate = dayOrderJson.getString("date");
                        StringTokenizer stk = new StringTokenizer(delivDate, "-");
                        if (stk.countTokens() != 3) {
                            Log.e("VTBD", "Day order date is not properly formatted : " + delivDate);
                            continue;
                        }

                        DayOrder newDayOrder = new DayOrder(uuid, delivDate, delivRouteUuid, firstName,
                                lastName, email, packageOrders, isCanceled, isDelivered, isPrepared);
                        dayOrders.add(newDayOrder);
                        Log.d("VTBD", "Got " + dayOrders.size() + " orders from response");
                        ListView lv = findViewById(R.id.dayOrdersAct_ordersList);
                        lv.setAdapter(new DayOrdersCustomList(thisAct, thisListener, dayOrders));
                    }
                }
                catch (JSONException exc) {
                    Log.e("VTBD", "Json parse exception for getDayPlannings : " + exc.toString());
                }
            }
        });
    }

    public void getQuery(String query, final EditDelivRouteActivity.HttpGetCallback callbackInterface) {
        Log.d("VTBD", "http query : " + query);

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, query,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("VTBD", "Response is : " + response);
                        if (callbackInterface != null) {
                            callbackInterface.onSuccess(response);
                        }
                    }
                },     new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VTBD", "Volley error : " + error.getMessage());
                try {
                    if (error.networkResponse!= null) {


                        int statusCode = error.networkResponse.statusCode;
                        if (error.networkResponse.data != null) {

                            String body = new String(error.networkResponse.data, "UTF-8");
                            if (statusCode == 400) {


                                JSONObject obj = new JSONObject(body);
                                String errorMsg = obj.getString("message");

                                // getting error msg message may be different according to your API
                                //Display this error msg to user
                                Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT).show();

                                Log.e("VTBD", "error message" + errorMsg);
                            }
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {

                    e.printStackTrace();
                    Log.e("VTBD", "UNKNOWN ERROR :" + e.getMessage());
                    Log.e("VTBD", "Headers :" + error.networkResponse.headers.toString());
                    try {
                        Log.e("VTBD", "Data bytes :" + new String(error.networkResponse.data));
                    } catch (Exception exc) {
                        Log.e("VTBD", "Cannot show byytes:" + exc.getMessage());
                    }
                    Toast.makeText(getApplicationContext(),"Something went Wrong!",Toast.LENGTH_SHORT).show();
                }
            }
        }
        );
        queue.add(stringRequest);
    }
}

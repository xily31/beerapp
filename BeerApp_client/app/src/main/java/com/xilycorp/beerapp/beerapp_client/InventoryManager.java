package com.xilycorp.beerapp.beerapp_client;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.xilycorp.beerapp.beerapp_client.BrowseBeerActivity.PREFS;
import static com.xilycorp.beerapp.beerapp_client.UrlManager.getPackagesUrl;
import static com.xilycorp.beerapp.beerapp_client.UrlManager.rootUrlInUse;

public class InventoryManager {

    SharedPreferences sharedPreferences;
    JSONArray inventoryJson;
    List<BeerPackage> inventory = new ArrayList<>();
    Context context;
    List<InventoryUpdateListener> listeners = new ArrayList<>();

    private static volatile InventoryManager sSoleInstance;

    public interface InventoryUpdateListener {
        void onInventoryUpdated();
    }

    public class BeerPackage {
        public String name;
        public String uuid;
        public String description;
        public String contentJson;
        public String price;

        public BeerPackage(String name, String uuid, String description, String contentJson, String price) {
            this.name = name;
            this.uuid = uuid;
            this.description = description;
            this.contentJson = contentJson;
            this.price = price;
        }
    }

    private InventoryManager(Context c){
        sharedPreferences = c.getSharedPreferences(PREFS, MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        context = c;
        if (inventory.isEmpty()) {
            updateInventory();
        }
    }

    public static InventoryManager getInstance(Context c, InventoryUpdateListener l){
        if (sSoleInstance == null){
            sSoleInstance = new InventoryManager(c);
        }
        sSoleInstance.listeners.add(l);
        return sSoleInstance;
    }

    public void notifyListeners() {
        for (InventoryUpdateListener l : listeners) {
            l.onInventoryUpdated();
        }
    }

    public BeerPackage getBeerPackageByUuid(String uuid) {
        for (BeerPackage bp : inventory) {
            if (bp.uuid.equalsIgnoreCase(uuid)) {
                return bp;
            }
        }
        return null;
    }

    public void updateInventory() {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = rootUrlInUse + getPackagesUrl;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                        //Log.d("VTBD", "Response is: "+ response.substring(0,500));
                        try {
                            // Parse JSON
                            JSONArray jsonResponse = new JSONArray(response);
                            inventoryJson = jsonResponse;
                            Log.d("VTBD", jsonResponse.toString());
                            if (inventory != null) {
                                inventory.clear();
                            }
                            else {
                                inventory = new ArrayList<>();
                            }
                            for (int i=0; i<jsonResponse.length(); i++) {
                                JSONObject beerPack = jsonResponse.getJSONObject(i);
                                String name = beerPack.getString("name");
                                String uuid = beerPack.getString("uuid");
                                String description = beerPack.getString("description");
                                String contentJson = beerPack.getString("content");
                                String price = beerPack.getString("price");
                                BeerPackage nbp = new BeerPackage(name, uuid, description, contentJson, price);
                                inventory.add(nbp);
                            }
                            Log.d("VTBD", "Updated inventory. Size : " + inventory.size());
                            notifyListeners();
                        }
                        catch (Throwable t) {
                            Log.d("VTBD", "JSON parsing error : " + t.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                Log.d("VTBD", "HTTP ERROR : " + error.toString());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}

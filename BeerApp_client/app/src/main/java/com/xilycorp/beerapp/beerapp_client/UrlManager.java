package com.xilycorp.beerapp.beerapp_client;

public abstract class UrlManager {
    // Root URL
    private final static String rootUrlStaging = "http://vps577075.ovh.net:8899/";
    private final static String rootUrlLocal = "http://192.168.1.59/";
    final static String rootUrlInUse = rootUrlStaging;

    // API URL
    final static String getPackagesUrl = "api/beerapp/getPackages.php";
    final static String getDelivroutesUrl = "api/beerapp/getDelivroutes.php";
    final static String pushOrderUrl = "api/beerapp/pushOrder.php";
}

package com.xilycorp.beerapp.beerapp_client;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class InventoryCustomList extends ArrayAdapter<InventoryManager.BeerPackage> {

private final Activity context;
private final List<InventoryManager.BeerPackage> inventoryItems;
private final InventoryEditListener listener;

    public interface InventoryEditListener {
        public void onInventoryItemChanged(String uuid, Integer newQuantity);
    }

    public InventoryCustomList(Activity context,
                               InventoryEditListener listener,
                          List<InventoryManager.BeerPackage> inventoryItems) {
        super(context, R.layout.content_cart_row, inventoryItems);
        this.context = context;
        this.inventoryItems = inventoryItems;
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.content_cart_row, null, true);
        TextView nameTv = (TextView) rowView.findViewById(R.id.cartRow_name);
        nameTv.setText(inventoryItems.get(position).name);

        // TODO : image of the delicious beer
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        //imageView.setImageResource(quantity[position]);

        final InventoryManager.BeerPackage rowItem = inventoryItems.get(position);
        CartManager.CartItem ci = CartManager.getInstance(context).getCartItemById(rowItem.uuid);
        final Integer quantity = (ci == null? 0 : ci.quantity);

        TextView quantityTv = (TextView) rowView.findViewById(R.id.cartRow_quantity);
        if (quantity > 0) {
            quantityTv.setText(Integer.toString(quantity));
        }
        else {
            quantityTv.setVisibility(View.INVISIBLE);
        }

        // Buttons
        Button addBtn = (Button) rowView.findViewById(R.id.cartRow_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked add button of inventory item " + rowItem.name);
                listener.onInventoryItemChanged(rowItem.uuid,1);
            }
        });
        Button subtractBtn = (Button) rowView.findViewById(R.id.cartRow_subtract);
        if (quantity <= 0) {
            subtractBtn.setEnabled(false);
        }
        else {
            subtractBtn.setEnabled(true);
        }
        subtractBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked remove button of inventory item " + rowItem.name);
                if (quantity > 0) {
                    listener.onInventoryItemChanged(rowItem.uuid, -1);
                }
            }
        });

        return rowView;
    }
}

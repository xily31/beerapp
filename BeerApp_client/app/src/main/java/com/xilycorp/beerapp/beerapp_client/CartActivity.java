package com.xilycorp.beerapp.beerapp_client;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import static com.xilycorp.beerapp.beerapp_client.BrowseBeerActivity.PREFS;
import static com.xilycorp.beerapp.beerapp_client.BrowseBeerActivity.PREFS_CART;

public class CartActivity extends AppCompatActivity {

    CartManager cartManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        cartManager = CartManager.getInstance(getBaseContext());
        Log.d("VTBD", "cart content : " + cartManager.cartJson.toString());

        updateCartItemsView();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartItemsView();
    }

    private void updateCartItemsView() {
        cartManager.populateCartListView(CartActivity.this, (ListView)findViewById(R.id.cartAct_listview));
    }

    public void onCheckoutClick(View v) {
        startActivity(new Intent(CartActivity.this, MapsActivity.class));
    }

    public void onClearCartClick(View v) {
        cartManager.resetCartContent();
        updateCartItemsView();
    }
}

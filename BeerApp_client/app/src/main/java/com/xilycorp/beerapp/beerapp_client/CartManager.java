package com.xilycorp.beerapp.beerapp_client;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.xilycorp.beerapp.beerapp_client.BrowseBeerActivity.PREFS;
import static com.xilycorp.beerapp.beerapp_client.BrowseBeerActivity.PREFS_CART;

public class CartManager
        implements InventoryManager.InventoryUpdateListener, CartCustomList.CartEditListener {

    private static volatile CartManager sSoleInstance;
    private InventoryManager inventoryManager;
    SharedPreferences sharedPreferences;
    JSONArray cartJson = null;
    List<CartItem> cartContent = new ArrayList<>();
    List<ListView> listViewsToKeepUpdated = new ArrayList();

    public class CartItem {
        public String name;
        public String uuid;
        public Integer quantity;

        public CartItem(String name, String uuid, Integer quantity) {
            this.name = name;
            this.uuid = uuid;
            this.quantity = quantity;
        }
    }

    private CartManager(Context c){
        sharedPreferences = c.getSharedPreferences(PREFS, MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        if (sharedPreferences.contains(PREFS_CART)) {
            cartJson = getCartJsonFromPrefs();
        } else {
            cartJson = new JSONArray();
        }
        inventoryManager = InventoryManager.getInstance(c, this);
    }

    @Override
    public void onInventoryUpdated() {

    }

    @Override
    public void onCartItemChanged(String uuid, Integer newQuantity) {
        CartItem foundItem = getCartItemById(uuid);
        if (foundItem != null) {
            foundItem.quantity = newQuantity;
        }
        updateCartJson();
        updateListViews();
    }

    private void updateListViews() {
        for (ListView v : listViewsToKeepUpdated) {
            v.setAdapter(new CartCustomList((Activity)v.getContext(), this, cartContent));
        }
    }

    public void resetCartContent() {
        sharedPreferences.edit().clear().apply(); // TODO : only clear PREFS_CART
        cartJson = new JSONArray();
        saveCart();
    }

    public JSONArray getCartJsonFromPrefs() {
        String cartJsonString = sharedPreferences.getString(PREFS_CART, "[]");
        try {
            JSONArray retArr = new JSONArray(cartJsonString);
            initCartContentFromPrefsJson();
            return retArr;
        }
        catch (Throwable t) {
            Log.e("VTBD", "Unable to parse saved cart JSON");
            return new JSONArray();
        }
    }

    public static CartManager getInstance(Context c){
        if (sSoleInstance == null){ //if there is no instance available... create new one
            sSoleInstance = new CartManager(c);
        }
        return sSoleInstance;
    }

    public Integer getNumberOfItemInCart(String packageId) {
        CartItem item = getCartItemById(packageId);
        if (item != null) {
            return item.quantity;
        }
        return 0;
    }

    public void setCartPrefJsonString(String content) {
        sharedPreferences.edit().
                putString(PREFS_CART, content)
                .apply();
    }

    public void saveCart() {
        setCartPrefJsonString(cartJson.toString());
        Log.d("VTBD", "Cart content is now " + cartJson.toString());
    }

    public CartItem getCartItemById(String packageId) {
        CartItem retItem = null;
        for (CartItem item : cartContent) {
            if (item.uuid.equalsIgnoreCase(packageId)) {
                retItem = item;
                break;
            }
        }
        return retItem;
    }

    private void initCartContentFromPrefsJson() {
        for (int i=0; i<this.cartJson.length(); i++) {
            try {
                JSONObject cartItem = this.cartJson.getJSONObject(i);
                String packUuid = cartItem.getString("beerPackId");
                Integer quantity = cartItem.getInt("quantity");
                addItemToCart(packUuid, quantity);
            }
            catch (Exception e) {
                Log.d("VTBD", "EXCEPTION " + e.toString());
                e.printStackTrace();
            }
        }
    }

    public void updateCartJson() {
        purgeNegativeCartItems();
        JSONArray newCartJsonArray = new JSONArray();
        for (CartItem item : cartContent) {
            JSONObject newItem = new JSONObject();
            try {
                newItem.put("beerPackId", item.uuid);
                newItem.put("quantity", item.quantity);
                newCartJsonArray.put(newItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cartJson = newCartJsonArray;
        saveCart();
        // TODO : notify listeners that cart content has changed
    }

    public void removeItemFromCart(String beerPackId) {
        addItemToCart(beerPackId, -1);
    }

    public void purgeNegativeCartItems() {
        // Clean cart of items with negative quantities
        List<CartItem> itemsToKeep = new ArrayList<>();
        for (CartItem item : cartContent) {
            if (item.quantity > 0) {
                itemsToKeep.add(item);
            }
        }
        cartContent = itemsToKeep;
    }

    public void addItemToCart(String beerPackId, Integer quantity) {

        // Check if item with uuid already exists in cart
        CartItem itemFound = null;
        for (CartItem item : cartContent) {
            if (item.uuid.equalsIgnoreCase(beerPackId)) {
                itemFound = item;
                break;
            }
        }
        // If so, only modify its quantity
        if (itemFound != null) {
            itemFound.quantity += quantity;
        }
        // Otherwise, create a new one if quantity is positive
        else {
            if (quantity > 0) {
                String packName = inventoryManager.getBeerPackageByUuid(beerPackId).name;
                CartItem newItem = new CartItem(packName, beerPackId, quantity);
                cartContent.add(newItem);
            }
        }
        updateCartJson();
    }

    public void populateCartListView(final Activity activity, ListView viewToPopulate) {

        listViewsToKeepUpdated.add(viewToPopulate);
        viewToPopulate.setAdapter(new CartCustomList(activity, this, cartContent));
    }
}

package com.xilycorp.beerapp.beerapp_client;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class CartCustomList extends ArrayAdapter<CartManager.CartItem> {

    private final Activity context;
    private final List<CartManager.CartItem> cartItems;
    private final CartEditListener listener;

    public interface CartEditListener {
        public void onCartItemChanged(String uuid, Integer newQuantity);
    }

    public CartCustomList(Activity context,
                            CartEditListener listener,
                            List<CartManager.CartItem> cartItems) {
        super(context, R.layout.content_cart_row, cartItems);
        this.context = context;
        this.cartItems = cartItems;
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.content_cart_row, null, true);
        TextView nameTv = (TextView) rowView.findViewById(R.id.cartRow_name);
        TextView quantityTv = (TextView) rowView.findViewById(R.id.cartRow_quantity);
        nameTv.setText(cartItems.get(position).name);
        quantityTv.setText(Integer.toString(cartItems.get(position).quantity));
        // TODO : image of the delicious beer
        //ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        //imageView.setImageResource(quantity[position]);

        final CartManager.CartItem rowItem = cartItems.get(position);

        // Buttons
        Button addBtn = (Button) rowView.findViewById(R.id.cartRow_add);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked add button of cart item " + rowItem.name);
                listener.onCartItemChanged(rowItem.uuid, rowItem.quantity + 1);
            }
        });
        Button subtractBtn = (Button) rowView.findViewById(R.id.cartRow_subtract);
        if (rowItem.quantity <= 0) {
            subtractBtn.setEnabled(false);
        }
        else {
            subtractBtn.setEnabled(true);
        }
        subtractBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VTBD", "Clicked remove button of cart item " + rowItem.name);
                if (rowItem.quantity > 0) {
                    listener.onCartItemChanged(rowItem.uuid, rowItem.quantity - 1);
                }
            }
        });

        return rowView;
    }
}

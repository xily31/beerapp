package com.xilycorp.beerapp.beerapp_client;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;


import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.xilycorp.beerapp.beerapp_client.UrlManager.getDelivroutesUrl;
import static com.xilycorp.beerapp.beerapp_client.UrlManager.rootUrlInUse;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public Button okBtn;
    public static Integer selectedDeliverySpot = null;
    public static DeliveryRoute selectedDelivroute = null;
    Calendar selectedDate = null;

    public class DeliverySpot {
        public String utcTime;
        public String address;

        public DeliverySpot(String utcTime, String address) {
            this.utcTime = utcTime;
            this.address = address;
        }
    }

    public class Checkpoint {
        public Double latitude;
        public Double longitude;
        public Integer number;
        public DeliverySpot deliverySpot;

        public Checkpoint(Integer nbr, Double lat, Double lon) {
            this.latitude = lat;
            this.longitude = lon;
            this.number = nbr;
            this.deliverySpot = null;
        }
    }

    public class DeliveryRoute implements
            GoogleMap.OnMarkerClickListener
    {
        String uuid = null;
        String date = null;

        public Checkpoint getCheckpointByLatLon(LatLng ll) {
            for (Checkpoint cp : checkpoints) {
                if (ll.latitude == cp.latitude && ll.longitude == cp.longitude){
                    return cp;
                }
            }
            return null;
        }

        public DeliverySpot getDeliverySpotByNumber(Integer nb) {
            for (Checkpoint cp : checkpoints) {
                if (cp.deliverySpot != null) {
                    if (cp.number == nb) {
                        return cp.deliverySpot;
                    }
                }
            }
            return null;
        }

        @Override
        public boolean onMarkerClick(Marker mk) {
            okBtn.setEnabled(true);
            Checkpoint cp = getCheckpointByLatLon(mk.getPosition());
            if (cp != null) {
                selectedDeliverySpot = cp.number;
                selectedDelivroute = this;
                Log.d("VTBD", "Selected checkpoint number " + selectedDeliverySpot);
            }
            return false;
        }

        public List<Checkpoint> checkpoints;

        public DeliveryRoute(List<Checkpoint> cps, String pUuid, String pDate) {
            checkpoints = cps;
            uuid = pUuid;
            date = pDate;
        }

        public void addDeliverySpot(Integer checkpointNumber, String time, String address) {
            for (Checkpoint cp : checkpoints) {
                if (cp.number.equals(checkpointNumber)) {
                    cp.deliverySpot = new DeliverySpot(time, address);
                    return;
                }
            }
        }

        public void displayOnMap(GoogleMap map) {
            map.clear(); // TODO : check that it clears stuff i drew on it, and markers

            // Show waypoints (route)
            List<LatLng> waypoints = new ArrayList<>();
            for (Checkpoint cp : checkpoints) {
                waypoints.add(new LatLng(cp.latitude, cp.longitude));
                if (cp.deliverySpot != null) {
                    LatLng markerCoords = new LatLng(cp.latitude, cp.longitude);
                    mMap.addMarker(new MarkerOptions().position(markerCoords).title("Livré à " + cp.deliverySpot.utcTime));
                }
            }
            mMap.setOnMarkerClickListener(this);

            Polyline polyline1 = mMap.addPolyline(new PolylineOptions()
                    .clickable(true));
            polyline1.setPoints(waypoints);
            String encodedWaypoints = PolyUtil.encode(waypoints);
            Log.d("STATE", "First waypoint " + waypoints.get(0).toString());
            Log.d("STATE", "Encoded waypoints : " + encodedWaypoints);
            Log.d("STATE", "Last waypoint " + waypoints.get(waypoints.size()-1).toString());

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng point : waypoints) {
                builder.include(point);
            }
            // Use this if we need to limit bounds of map
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 10));

        }
    }

    private void updateSelectedDate() {
        TextView dateView = findViewById(R.id.datePicker);
        DateFormat sdf = SimpleDateFormat.getDateInstance();
        String currentDate = sdf.format(selectedDate.getTime());
        dateView.setText(currentDate);

        // TODO : if day is currentDay, gray out previous button
        if (selectedDate.getTime().compareTo(new Date()) == 0) {
            
        }
    }

    public void onPrevClicked(View v) {
        selectedDate.add(Calendar.DAY_OF_YEAR, -1);
        updateSelectedDate();
    }

    public void onNextClicked(View v) {
        selectedDate.add(Calendar.DAY_OF_YEAR, 1);
        updateSelectedDate();
    }

    public void onValidateClicked(View v) {
        Log.d("VTBD", "Ordering on day " + selectedDelivroute.date +
                " at delivery spot " +
                selectedDelivroute.getDeliverySpotByNumber(selectedDeliverySpot).utcTime
        );

        // TODO : Launch existing order summary activity if exists
        Intent intent = new Intent(MapsActivity.this, OrderSummaryActivity.class);
        intent.putExtra("year", selectedDate.get(Calendar.YEAR));
        intent.putExtra("month", selectedDate.get(Calendar.MONTH) + 1);
        intent.putExtra("day", selectedDate.get(Calendar.DAY_OF_MONTH));
        Log.d("VTBD", "Ordering for delivery date on " + selectedDate.getTime().toString());
        startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        okBtn = findViewById(R.id.ok_btn);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        selectedDate = calendar;
        updateSelectedDate();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getDelivRoute();
    }

    public void getDelivRoute() {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        String currentDate = sdf.format(tomorrow);

        Log.d("VTBD", "curernt date string : "  + currentDate.toString());
        String url = rootUrlInUse + getDelivroutesUrl;
        url += "?date=" + currentDate;
        Log.d("VTBD", "url query : " + url);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VTBD", "getDelivroutes response is: " + response);
                        try {
                            // Parse root JSON
                            JSONObject rootObj = new JSONObject(response);

                            // Delivery route json content
                            JSONObject delivRouteContentJson = rootObj.getJSONObject("content");
                            String delivRouteUuid = rootObj.getString("uuid");
                            String delivrouteDate = rootObj.getString("date");

                            // Checkpoints
                            List<Checkpoint> checkpoints = new ArrayList<>();
                            JSONArray checkpointsArr = delivRouteContentJson.getJSONArray("checkpoints");
                            for (int i=0; i<checkpointsArr.length(); ++i) {
                                JSONObject cpObj = checkpointsArr.getJSONObject(i);
                                Log.d("VTBD", "Checkpoint : " + cpObj.toString());
                                Integer nbr = cpObj.getInt("number");
                                Double lat = cpObj.getDouble("latitude");
                                Double lon = cpObj.getDouble("longitude");
                                checkpoints.add(new Checkpoint(nbr, lat, lon));
                            }

                            // Init new delivery route
                            DeliveryRoute delivRoute = new DeliveryRoute(
                                    checkpoints,
                                    delivRouteUuid,
                                    delivrouteDate);

                            // Delivery spots
                            List<Checkpoint> delivSpots = new ArrayList<>();
                            JSONArray delivSpotsArr = delivRouteContentJson.getJSONArray("deliverySpots");
                            for (int i=0; i<delivSpotsArr.length(); ++i) {
                                JSONObject dsObj = delivSpotsArr.getJSONObject(i);
                                Log.d("VTBD", "Deliv spot : " + dsObj.toString());
                                Integer delivspotNumber = dsObj.getInt("checkpointNumber");
                                String delivTime = dsObj.getString("utcTime");
                                String address = dsObj.getString("address");
                                delivRoute.addDeliverySpot(delivspotNumber, delivTime, address);
                            }

                            // Display new delivery route
                            delivRoute.displayOnMap(mMap);
                        }
                        catch (Throwable t) {
                            Log.d("VTBD", "JSON parsing error : " + t.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                Log.d("VTBD", "HTTP ERROR : " + error.toString());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}

package com.xilycorp.beerapp.beerapp_client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import static com.xilycorp.beerapp.beerapp_client.UrlManager.pushOrderUrl;
import static com.xilycorp.beerapp.beerapp_client.UrlManager.rootUrlInUse;

public class OrderSummaryActivity extends AppCompatActivity {

    CartManager cartManager;
    EditText firstNameInput;
    EditText lastNameInput;
    EditText emailInput;
    Button orderBtn;
    Boolean orderSuccessful = false;
    int delivDay = -1;
    int delivYear = -1;
    int delivMonth = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        setupTextInputs();
        cartManager = CartManager.getInstance(getBaseContext());
        updateCartItemsView();
        updateDeliveryViews();
        getOrderDateFromIntent();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateCartItemsView();
        updateDeliveryViews();
        getOrderDateFromIntent();
    }

    private void getOrderDateFromIntent() {
        Intent intent = getIntent();
        delivYear = intent.getIntExtra("year", -1);
        delivMonth = intent.getIntExtra("month", -1);
        delivDay = intent.getIntExtra("day", -1);
        if (delivDay == -1 || delivMonth == -1 || delivYear == -1) {
            Log.e("VTBD", "Delivery date is wrong : " + Integer.toString(delivDay) + "/" +
                    Integer.toString(delivMonth) + "/" + Integer.toString(delivYear));
        } else {
            Log.d("VTBD", "Delivery date is : " + Integer.toString(delivDay) + "/" +
                    Integer.toString(delivMonth) + "/" + Integer.toString(delivYear));
        }
    }

    private void setupTextInputs() {
        firstNameInput = this.findViewById(R.id.firstname_input);
        lastNameInput = this.findViewById(R.id.lastname_input);
        emailInput = this.findViewById(R.id.email_input);
        orderBtn = this.findViewById(R.id.orderBtn);
        TextWatcher tw = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                Boolean enable = (firstNameInput.length() > 0 &&
                        lastNameInput.length() > 0 &&
                        emailInput.length() > 0 &&
                        !orderSuccessful);
                orderBtn.setEnabled(enable);
            }
        };
        firstNameInput.addTextChangedListener(tw);
        lastNameInput.addTextChangedListener(tw);
        emailInput.addTextChangedListener(tw);
    }

    private void updateCartItemsView() {
        cartManager.populateCartListView(
                OrderSummaryActivity.this,
                (ListView)findViewById(R.id.orderAct_cartListView));
    }

    private void updateDeliveryViews() {
        try {
            // Get delivery data
            MapsActivity.DeliveryRoute delivRoute = MapsActivity.selectedDelivroute;
            Integer spotId = MapsActivity.selectedDeliverySpot;
            MapsActivity.DeliverySpot spot = delivRoute.getDeliverySpotByNumber(spotId);

            // Address
            TextView delivAddrView = findViewById(R.id.deliverySpot_textView);
            String textToShow = spot.address;
            delivAddrView.setText(textToShow);

            // Date time
            EditText delivDateView = findViewById(R.id.deliveryDate_textView);
            String dateTimeToShow = delivRoute.date + " à " + spot.utcTime;
            delivDateView.setText(dateTimeToShow);
        }
        catch (Exception e) {
            Log.d("VTBD", "EXCEPTION");
            e.printStackTrace();
        }
    }

    public void onOrderClicked(View v) {
        // Get delivery data
        MapsActivity.DeliveryRoute delivRoute = MapsActivity.selectedDelivroute;
        Integer spotId = MapsActivity.selectedDeliverySpot;
        MapsActivity.DeliverySpot spot = delivRoute.getDeliverySpotByNumber(spotId);
        String delivrouteUuid = delivRoute.uuid;
        String orderJson = cartManager.cartJson.toString();
        pushOrder(spotId.toString(), delivrouteUuid, orderJson);
    }

    private void pushOrder(String delivSpotNbr, String delivrouteUuid, String orderJson) {
        Log.d("VTBD", "Pushing order with delivSpotNbr : " + delivSpotNbr +
                ", delivrouteUuid : " + delivrouteUuid +
                ", orderJson : " + orderJson);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = rootUrlInUse + pushOrderUrl;

        url += "?delivSpotNbr=" + delivSpotNbr;
        url += "&delivrouteUuid=" + delivrouteUuid;
        url += "&orderJson=" + orderJson;
        url += "&firstName=" + firstNameInput.getText().toString().trim();
        url += "&lastName=" + lastNameInput.getText().toString().trim();
        url += "&email=" + emailInput.getText().toString().trim();
        url += "&deliveryDay=" + Integer.toString(delivYear) + "-" +
                Integer.toString(delivMonth) + "-" + Integer.toString(delivDay);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                // Display the first 500 characters of the response string.
                Log.d("VTBD", "Response is: " + response);
                Button orderBtn = findViewById(R.id.orderBtn);
                orderBtn.setEnabled(false);
                cartManager.cartContent.clear();
                orderSuccessful = true;
                Intent intent = new Intent(getApplicationContext(),MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                String toastMsg = "Merci de votre commande! Vous recevrez un email sous peu.";
                intent.putExtra("toast", toastMsg);
                startActivity(intent);
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                Log.d("VTBD", "HTTP ERROR : " + error.toString());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}

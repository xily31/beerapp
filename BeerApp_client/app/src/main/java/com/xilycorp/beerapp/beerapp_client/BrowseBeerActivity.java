package com.xilycorp.beerapp.beerapp_client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class BrowseBeerActivity extends AppCompatActivity
        implements InventoryManager.InventoryUpdateListener, InventoryCustomList.InventoryEditListener{

    Button addButton, removeButton = null;
    LinearLayout beersList = null;
    public static final String PREFS = "PREFS";
    public static final String PREFS_CART = "PREFS_CART";
    String selectedPackage = null;
    CartManager cartManager = null;
    InventoryManager inventoryManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_beer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cartManager = CartManager.getInstance(getBaseContext());
        inventoryManager = InventoryManager.getInstance(getBaseContext(), this);
    }

    @Override
    public void onResume() {
        super.onResume();
        populateBeerList();
    }

    @Override
    public void onInventoryUpdated() {
        Log.d("VTBD", this.getClass().toString() + " : onInventoryUpdated");
        populateBeerList();
    }

    @Override
    public void onInventoryItemChanged(String uuid, Integer newQuantity) {
        cartManager.addItemToCart(uuid, newQuantity);
        populateBeerList();
    }

    public void onCartClicked(View v) {
        startActivity(new Intent(BrowseBeerActivity.this, CartActivity.class));
    }

    /*
    private void setupOnclicks() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPackage != null) {
                    cartManager.addItemToCart(selectedPackage, 1);
                    updateAddRemoveButtons();
                }
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPackage != null) {
                    cartManager.removeItemFromCart(selectedPackage);
                    updateAddRemoveButtons();
                }
            }
        });
    }


    private List<View> getPackageLayouts() {
        List<View> packageLayouts = new ArrayList<>();
        for (int i = 0; i < beersList.getChildCount(); ++i) {
            View packageLayout = beersList.getChildAt(i);
            if (packageLayout instanceof LinearLayout) {
                packageLayouts.add(packageLayout);
            }
        }
        return packageLayouts;
    }

    // Update add and remove buttons according to selection
    private void updatePackageSelection(View v) {
        selectedPackage = null;
        for (View packageView : getPackageLayouts()) {
            if (packageView.isSelected()) {
                packageView.setBackgroundColor(0x8800FF00);
                View pkgLayout = ((LinearLayout) packageView).getChildAt(1);
                if (pkgLayout instanceof TextView) {
                    selectedPackage = ((TextView)pkgLayout).getText().toString();
                }
            } else {
                packageView.setBackgroundColor(0x00000000);
            }
        }
        updateAddRemoveButtons();
    }

    private void updateAddRemoveButtons() {
        boolean isBeerSelected = (selectedPackage != null);
        addButton.setEnabled(isBeerSelected);
        if (cartManager.getNumberOfItemInCart(selectedPackage) > 0) {
            removeButton.setEnabled(isBeerSelected);
        } else {
            removeButton.setEnabled(false);
        }
    }
    */

    // Populates list with beers from web API
    private void populateBeerList() {
        try {
            ListView viewToPopulate = findViewById(R.id.browserAct_listview);
            viewToPopulate.setAdapter(new InventoryCustomList(
                    BrowseBeerActivity.this, this, inventoryManager.inventory));

            /*beersList.removeAllViews();
            List<InventoryManager.BeerPackage> beerPackages = inventoryManager.inventory;
            List<LinearLayout> layouts = new ArrayList<>();
            for (InventoryManager.BeerPackage bp : beerPackages) {
                LinearLayout packageLayout = new LinearLayout(BrowseBeerActivity.this);
                packageLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                ));

                TextView packageNameView = new TextView(BrowseBeerActivity.this);
                packageNameView.setLayoutParams(new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        72
                ));
                packageNameView.setText(bp.name);

                TextView packageUuidView = new TextView(BrowseBeerActivity.this);
                packageUuidView.setLayoutParams(new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        72
                ));
                packageUuidView.setVisibility(View.INVISIBLE);
                packageUuidView.setText(bp.uuid);

                packageLayout.addView(packageNameView);
                packageLayout.addView(packageUuidView);
                packageLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (View packageView : getPackageLayouts()) {
                            if (packageView == v) continue;
                            packageView.setSelected(false);
                        }
                        v.setSelected(!v.isSelected());
                        updatePackageSelection(v);
                    }
                });
                layouts.add(packageLayout);

            }

            // Populate list
            for (LinearLayout beerLayout : layouts) {
                if (beerLayout != null) {
                    beersList.addView(beerLayout);
                }
            }*/
        }
        catch (Exception e) {
            Log.e("VTBD", "Exception : " + e.toString());
            e.printStackTrace();
        }
    }
}
